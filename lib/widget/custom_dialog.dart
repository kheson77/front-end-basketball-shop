import 'package:flutter/material.dart';

class CustomDialog extends StatelessWidget {
  final VoidCallback onConfirm;
  final String actionText;
  final String image;
  final String name;
  final String title;

  CustomDialog.product(
      {@required this.onConfirm,
      @required this.image,
      @required this.name,
      this.actionText})
      : this.title = "sản phẩm";

  CustomDialog.account(
      {@required this.onConfirm,
      @required this.image,
      @required this.name,
      this.actionText})
      : this.title = "tài khoản";

  CustomDialog.news(
      {@required this.onConfirm,
      @required this.image,
      @required this.name,
      this.actionText})
      : this.title = "bài viết";
  CustomDialog.user(
      {@required this.onConfirm,
      @required this.image,
      @required this.name,
      this.actionText})
      : this.title = "thông tin người dùng";

  @override
  Widget build(BuildContext context) {
    var width = double.infinity;
    return Dialog(
        backgroundColor: Colors.transparent,
        insetPadding: EdgeInsets.all(10),
        child: Stack(
          overflow: Overflow.visible,
          alignment: Alignment.center,
          children: <Widget>[
            Container(
              width: width,
              height: 220,
              decoration: BoxDecoration(
                  // border: Border.all(color: Colors.red, width: 2),
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.white),
              padding: EdgeInsets.fromLTRB(20, 50, 20, 20),
              child: Column(
                children: [
                  Text(this.name),
                  Container(
                    padding: EdgeInsets.only(top: 5),
                    child: Text(
                        "Bạn có chắc chắn muốn ${this.actionText} ${this.title} ?",
                        style: TextStyle(fontSize: 20),
                        textAlign: TextAlign.center),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 30),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        FlatButton(
                          onPressed: onConfirm,
                          child: Text(
                            "Đồng ý",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: Colors.blue,
                        ),
                        SizedBox(width: 20),
                        FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            color: Colors.grey,
                            child: Text(
                              "Hủy",
                              style: TextStyle(color: Colors.white),
                            ))
                      ],
                    ),
                  )
                ],
              ),
            ),
            Positioned(
              top: -100,
              child: Container(
                height: 150,
                width: 150,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: this.image == null
                            ? AssetImage('assets/default_image.jpg')
                            : NetworkImage(this.image),
                        fit: BoxFit.cover)),
              ),
            )
          ],
        ));
  }
}
