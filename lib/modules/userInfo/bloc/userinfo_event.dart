part of 'userinfo_bloc.dart';

@immutable
abstract class UserinfoEvent {}

class UserInfoFirstLoadEvent extends UserinfoEvent {}

class UserInfoLoaded extends UserinfoEvent {
  // final int idUser;
  // final String accessToken;
  // UserInfoLoaded(this.idUser, this.accessToken);
}

class UserInfoUpdateImageEvent extends UserinfoEvent {
  final String image;
  UserInfoUpdateImageEvent(this.image);
}

class UserInfoUpdateInfoEvent extends UserinfoEvent {
  final User user;
  UserInfoUpdateInfoEvent(this.user);
}
