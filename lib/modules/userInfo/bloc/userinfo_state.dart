part of 'userinfo_bloc.dart';

@immutable
abstract class UserinfoState {
  final User user;
  UserinfoState({this.user});
}

class UserinfoInitial extends UserinfoState {}

class UserInfoLoad extends UserinfoState {
  UserInfoLoad(User user) : super(user: user);
}

class UserInfoSuccess extends UserinfoState {
  UserInfoSuccess(User user) : super(user: user);
}
