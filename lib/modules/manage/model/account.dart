// To parse this JSON data, do
//
//     final accountModel = accountModelFromJson(jsonString);

import 'dart:convert';

AccountModel accountModelFromJson(String str) =>
    AccountModel.fromJson(json.decode(str));

String accountModelToJson(AccountModel data) => json.encode(data.toJson());

class AccountModel {
  AccountModel({
    this.count,
    this.next,
    this.previous,
    this.results,
  });

  int count;
  dynamic next;
  dynamic previous;
  List<Account> results;

  factory AccountModel.fromJson(Map<String, dynamic> json) => AccountModel(
        count: json["count"],
        next: json["next"],
        previous: json["previous"],
        results:
            List<Account>.from(json["results"].map((x) => Account.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "next": next,
        "previous": previous,
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
      };
}

class Account {
  Account({
    this.idUser,
    this.firstName,
    this.lastName,
    this.email,
    this.userThumbnail,
    this.gender,
    this.phoneNumber,
    this.modifiedDate,
    this.userType,
  });

  int idUser;
  String firstName;
  String lastName;
  String email;
  String userThumbnail;
  String gender;
  int phoneNumber;
  DateTime modifiedDate;
  String userType;

  factory Account.fromJson(Map<String, dynamic> json) => Account(
        idUser: json["idUser"],
        firstName: json["firstName"],
        lastName: json["lastName"] == null ? null : json["lastName"],
        email: json["email"],
        userThumbnail: json["userThumbnail"],
        gender: json["gender"] == null ? null : json["gender"],
        phoneNumber: json["phoneNumber"] == null ? null : json["phoneNumber"],
        modifiedDate: DateTime.parse(json["modifiedDate"]),
        userType: json["userType"],
      );

  Map<String, dynamic> toJson() => {
        "idUser": idUser,
        "firstName": firstName,
        "lastName": lastName == null ? null : lastName,
        "email": email,
        "userThumbnail": userThumbnail,
        "gender": gender == null ? null : gender,
        "phoneNumber": phoneNumber == null ? null : phoneNumber,
        "modifiedDate": modifiedDate.toIso8601String(),
        "userType": userType,
      };
}
