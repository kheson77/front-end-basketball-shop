import 'package:basketballShop/Core/routes.dart';
import 'package:basketballShop/const/color_custom.dart';
import 'package:basketballShop/modules/login/bloc/login_bloc.dart';
import 'package:basketballShop/modules/product/bloc/product_bloc.dart';
import 'package:basketballShop/modules/product/models/product.dart';
import 'package:basketballShop/modules/shopping_cart/bloc/cart_bloc.dart';
import 'package:basketballShop/widget/custom_button.dart';
import 'package:basketballShop/widget/product_detail/select_color.dart';
import 'package:basketballShop/widget/product_detail/select_size.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fpt_component/widgets/segmented_control.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class ProductDetail extends StatefulWidget {
  ProductDetail({@required this.product});
  Product product;

  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  String caruselPic1 =
      "https://static.highsnobiety.com/thumbor/mRXE3Qge1OUq_Ir1STZkHK9E5lM=/1600x1067/static.highsnobiety.com/wp-content/uploads/2019/05/01114150/best-basketball-shoes-main.jpg";
  String caruselPic2 =
      "https://images-na.ssl-images-amazon.com/images/I/61PHITYIxWL._AC_UY395_.jpg";

  int _indexCount;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _indexCount = 1;
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    final CartBloc bloc = BlocProvider.of<CartBloc>(context);

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(widget.product.name),
        backgroundColor: Colors.white,
        actions: [
          IconButton(
            icon: Icon(Icons.more_vert),
            tooltip: 'Open shopping cart',
            onPressed: () {
              // handle the press
            },
            color: Colors.black,
          ),
        ],
        leading: IconButton(
          icon: Icon(Icons.arrow_back_outlined),
          color: Colors.black,
          onPressed: () {
            // Get.back();
            CoreRoutes.instance.pop();
          },
        ),
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                    padding:
                        const EdgeInsets.only(top: 12, left: 12, right: 12),
                    child: Text(
                      widget.product.name,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                      textDirection: TextDirection.ltr,
                    )),
                Hero(
                  tag: widget.product.idProduct,
                  child: Container(
                    padding: EdgeInsets.only(top: 12, left: 12, right: 12),
                    child: Container(
                        width: width,
                        height: height / 3.5,
                        child: SizedBox(
                          child: Carousel(
                            dotSize: 4.0,
                            dotSpacing: 15.0,
                            dotColor: primaryColor,
                            indicatorBgPadding: 5.0,
                            dotBgColor: Colors.transparent,
                            dotIncreasedColor: Colors.orange,
                            images: [
                              CachedNetworkImage(
                                imageUrl: widget.product.thumbnail,
                                fit: BoxFit.cover,
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                placeholder: (context, url) =>
                                    Center(child: CircularProgressIndicator()),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                              CachedNetworkImage(
                                imageUrl: caruselPic1,
                                fit: BoxFit.cover,
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                placeholder: (context, url) =>
                                    Center(child: CircularProgressIndicator()),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                              CachedNetworkImage(
                                imageUrl: caruselPic2,
                                fit: BoxFit.cover,
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                placeholder: (context, url) =>
                                    Center(child: CircularProgressIndicator()),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              )
                            ],
                          ),
                        )),
                  ),
                ),
                Container(
                    margin: const EdgeInsets.only(top: 10, left: 12, right: 12),
                    // width: 150,
                    height: 50,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.black.withOpacity(0.6)),
                    child: Row(
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Text(
                            widget.product.price.toString() + ' đ',
                            style: TextStyle(fontSize: 20, color: Colors.white),
                          ),
                        ),
                        Spacer(),
                        Container(
                          padding: EdgeInsets.only(right: 10),
                          child: SmoothStarRating(
                            rating: 4.5,
                            size: 20,
                            color: Colors.amber[200],
                            borderColor: Colors.white,
                            starCount: 5,
                          ),
                        ),
                      ],
                    )),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            padding: const EdgeInsets.only(
                                top: 10, left: 12, right: 12),
                            child: Text(
                              "Màu sắc",
                              style: TextStyle(fontSize: 16),
                            )),
                        SelectColor(),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            padding: const EdgeInsets.only(
                                top: 10, left: 12, right: 12),
                            child: Text(
                              "Số lượng",
                              style: TextStyle(fontSize: 16),
                            )),
                        Container(
                            height: 40,
                            width: 130,
                            margin: const EdgeInsets.only(
                                top: 5, left: 22, right: 12),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.grey.withOpacity(0.3)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                IconButton(
                                  icon: Icon(Icons.arrow_left),
                                  color: _indexCount == 1
                                      ? Colors.grey
                                      : Colors.black,
                                  onPressed: () {
                                    setState(() {
                                      if (_indexCount > 1) {
                                        _indexCount--;
                                      }
                                    });
                                  },
                                ),
                                Text(
                                  "${_indexCount}",
                                  style: TextStyle(color: Colors.black),
                                ),
                                IconButton(
                                  icon: Icon(Icons.arrow_right),
                                  color: Colors.black,
                                  onPressed: () {
                                    setState(() {
                                      _indexCount++;
                                    });
                                  },
                                ),
                              ],
                            )),
                      ],
                    ),
                  ],
                ),
                Container(
                    padding:
                        const EdgeInsets.only(top: 10, left: 12, right: 12),
                    child: Text(
                      "Kích cỡ",
                      style: TextStyle(fontSize: 16),
                    )),
                SelectSize(),
                Container(
                    padding:
                        const EdgeInsets.only(top: 10, left: 12, right: 12),
                    child: Text(
                      "Mô tả sản phẩm",
                      style: TextStyle(fontSize: 16),
                    )),
                Padding(
                    padding: const EdgeInsets.only(top: 5, left: 12, right: 12),
                    child: Text(
                      widget.product.description,
                      style:
                          TextStyle(fontSize: 14, fontStyle: FontStyle.italic),
                      textDirection: TextDirection.ltr,
                    )),
                BlocBuilder<LoginBloc, LoginState>(
                  builder: (context, state) {
                    return Container(
                        padding: EdgeInsets.only(top: 20, bottom: 10),
                        child: CustomButton("Thêm vào giỏ hàng", () {
                          if (state is LoginSuccessful) {
                            bloc.add(AddItemEvent(widget.product, _indexCount));
                          } else {
                            _scaffoldKey.currentState.showSnackBar(SnackBar(
                                content: Text(
                                    "Vui lòng đăng nhập để thực hiện chức năng này!")));
                          }
                        }, Colors.blue));
                  },
                ),
                Container(
                  padding: const EdgeInsets.only(left: 12, right: 12),
                  child: Divider(
                    height: 12,
                    thickness: 1,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 12, right: 12, top: 8),
                  child: Text(
                    "SẢN PHẨM LIÊN QUAN",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                BlocBuilder<ProductBloc, ProductState>(
                  builder: (context, state) {
                    return SizedBox(
                      height: height / 4,
                      child: ListView.builder(
                        padding: EdgeInsets.only(
                            top: 10, left: 8, right: 8, bottom: 16),
                        scrollDirection: Axis.horizontal,
                        itemCount: state.relatedProduct.length,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              BlocProvider.of<ProductBloc>(context).add(
                                  RelatedProductEvent(
                                      state.relatedProduct[index].idProduct));
                              CoreRoutes.instance.navigateAndReplace(
                                  CoreRouteNames.PRODUCT_DETAIL,
                                  arguments: state.relatedProduct[index]);
                            },
                            child: Container(
                              padding: EdgeInsets.only(
                                  top: 10, left: 8, right: 8, bottom: 16),
                              child: Column(
                                children: [
                                  Container(
                                    height: height / 8,
                                    width: width / 3,
                                    child: Container(
                                        decoration: BoxDecoration(
                                      image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: NetworkImage(state
                                              .relatedProduct[index]
                                              .thumbnail)),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(8.0)),
                                    )),
                                  ),
                                  Container(
                                    // height: Get.height / 12,
                                    padding: EdgeInsets.only(top: 4),
                                    width: width / 3,
                                    child: Text(
                                      state.relatedProduct[index].name,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                  Container(
                                    // height: Get.height / 12,
                                    padding: EdgeInsets.only(top: 4),
                                    width: width / 3,
                                    child: Text(
                                      "Giá: " +
                                          state.relatedProduct[index].price
                                              .toString(),
                                      style: TextStyle(fontSize: 12),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  },
                )
              ],
            )),
      ),
    );
  }
}
