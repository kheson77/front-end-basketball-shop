import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  CustomButton(this.text, this.onPress, this.color) : outline = null;
  CustomButton.outline(this.text, this.onPress, this.color) : outline = true;

  String text;
  Function onPress;
  Color color;
  bool outline;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Container(
        width: width,
        height: 60,
        padding: EdgeInsets.only(left: 20, right: 20, bottom: 12),
        child: this.outline == null
            ? RaisedButton(
                onPressed: onPress,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                color: color,
                child: Text(
                  this.text,
                  style: TextStyle(color: Colors.white, fontSize: 15),
                ),
              )
            : FlatButton(
                onPressed: onPress,
                shape: RoundedRectangleBorder(
                    side: BorderSide(color: color, width: 1.5),
                    borderRadius: BorderRadius.circular(8)),
                child: Text(
                  this.text,
                  style: TextStyle(color: color, fontSize: 15),
                ),
              ));
  }
}
