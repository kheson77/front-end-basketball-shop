part of 'manage_bloc.dart';

@immutable
abstract class ManageState {
  final List<Account> accounts;
  final List<Product> products;
  final List<Cart> carts;
  final List<News> news;

  // shoe
  List<Product> get shoes {
    if (products == null || products.length == 0) {
      return [];
    }
    List<Product> shoes = [];
    for (var i = 0; i < products.length; i++) {
      if (products[i].idCategory == 5) {
        shoes.add(products[i]);
      }
    }
    return shoes;
  }

  // jersey
  List<Product> get jerseys {
    if (products == null || products.length == 0) {
      return [];
    }
    List<Product> jerseys = [];
    for (var i = 0; i < products.length; i++) {
      if (products[i].idCategory == 6) {
        jerseys.add(products[i]);
      }
    }
    return jerseys;
  }

  // accessory
  List<Product> get accessories {
    if (products == null || products.length == 0) {
      return [];
    }
    List<Product> accessories = [];
    for (var i = 0; i < products.length; i++) {
      if (products[i].idCategory == 7) {
        accessories.add(products[i]);
      }
    }
    return accessories;
  }

  // basketball
  List<Product> get basketball {
    if (products == null || products.length == 0) {
      return [];
    }
    List<Product> basketball = [];
    for (var i = 0; i < products.length; i++) {
      if (products[i].idCategory == 8) {
        basketball.add(products[i]);
      }
    }
    return basketball;
  }

  // customer
  List<Account> get customers {
    if (accounts == null || accounts.length == 0) {
      return [];
    }
    List<Account> customers = [];
    for (var i = 0; i < accounts.length; i++) {
      if (accounts[i].userType == 'customer') {
        customers.add(accounts[i]);
      }
    }
    return customers;
  }

  // manager
  List<Account> get managers {
    if (accounts == null || accounts.length == 0) {
      return [];
    }
    List<Account> managers = [];
    for (var i = 0; i < accounts.length; i++) {
      if (accounts[i].userType == 'manager') {
        managers.add(accounts[i]);
      }
    }
    return managers;
  }

  ManageState({this.accounts, this.products, this.carts, this.news});
}

class ManageInitial extends ManageState {}

class ManageLoad extends ManageState {
  ManageLoad(List<Account> accounts, List<Product> products, List<Cart> carts,
      List<News> news)
      : super(accounts: accounts, products: products, carts: carts, news: news);
}

class ManageSuccess extends ManageState {
  ManageSuccess(List<Account> accounts, List<Product> products,
      List<Cart> carts, List<News> news)
      : super(accounts: accounts, products: products, carts: carts, news: news);
}
