part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class LoginFirstLoadEvent extends LoginEvent {}

class LoginUsernameChanged extends LoginEvent {
  final String username;

  LoginUsernameChanged({this.username});
}

class LoginPasswordChanged extends LoginEvent {
  final String password;

  LoginPasswordChanged({this.password});
}

class LoginSubmitted extends LoginEvent {
  final String username;
  final String password;
  LoginSubmitted({this.username, this.password});
}

class UpdateImage extends LoginEvent {
  final String image;
  UpdateImage({this.image});
}

class LogoutEvent extends LoginEvent {}
