part of 'cart_bloc.dart';

@immutable
abstract class CartEvent {}

class FirstLoadCartEvent extends CartEvent {}

class LoadedCart extends CartEvent {
  final int idUser;
  final String accessToken;
  LoadedCart(this.idUser, this.accessToken);
}

class AddItemEvent extends CartEvent {
  final Product product;
  final int count;
  AddItemEvent(this.product, this.count);
}

class IncreaseItemEvent extends CartEvent {
  final Cart cart;
  IncreaseItemEvent(this.cart);
}

class DecreaseItemEvent extends CartEvent {
  final Cart cart;
  DecreaseItemEvent(this.cart);
}

class DeleteItemEvent extends CartEvent {
  final int index;
  DeleteItemEvent(this.index);
}
