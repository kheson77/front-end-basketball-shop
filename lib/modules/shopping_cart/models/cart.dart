// To parse this JSON data, do
//
//     final cartModel = cartModelFromJson(jsonString);

import 'dart:convert';

CartModel cartModelFromJson(String str) => CartModel.fromJson(json.decode(str));

String cartModelToJson(CartModel data) => json.encode(data.toJson());

class CartModel {
  CartModel({
    this.count,
    this.next,
    this.previous,
    this.results,
  });

  int count;
  dynamic next;
  dynamic previous;
  List<Cart> results;

  factory CartModel.fromJson(Map<String, dynamic> json) => CartModel(
        count: json["count"],
        next: json["next"],
        previous: json["previous"],
        results: List<Cart>.from(json["results"].map((x) => Cart.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "next": next,
        "previous": previous,
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
      };
}

class Cart {
  Cart({
    this.idCart,
    this.idUser,
    this.idProduct,
    this.idCategory,
    this.nameProduct,
    this.thumbnailProduct,
    this.priceProduct,
    this.dateModified,
    this.quantity,
  });

  int idCart;
  int idUser;
  int idProduct;
  int idCategory;
  String nameProduct;
  String thumbnailProduct;
  double priceProduct;
  DateTime dateModified;
  int quantity;

  factory Cart.fromJson(Map<String, dynamic> json) => Cart(
        idCart: json["idCart"],
        idUser: json["idUser"],
        idProduct: json["idProduct"],
        idCategory: json["idCategory"],
        nameProduct: json["nameProduct"],
        thumbnailProduct: json["thumbnailProduct"],
        priceProduct: double.parse(json["priceProduct"]),
        dateModified: DateTime.parse(json["dateModified"]),
        quantity: json["quantity"],
      );

  Map<String, dynamic> toJson() => {
        "idCart": idCart,
        "idUser": idUser,
        "idProduct": idProduct,
        "idCategory": idCategory,
        "nameProduct": nameProduct,
        "thumbnailProduct": thumbnailProduct,
        "priceProduct": priceProduct,
        "dateModified": dateModified.toIso8601String(),
        "quantity": quantity,
      };
}
