import 'package:get/state_manager.dart';

class TabbarController extends GetxController {
  var index = 0.obs;
  void change(index) {
    this.index.value = index;
  }
}
