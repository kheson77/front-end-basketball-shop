// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  User({
    this.idUser,
    this.firstName,
    this.lastName,
    this.email,
    this.userThumbnail,
    this.gender,
    this.phoneNumber,
    this.modifiedDate,
    this.userType,
    this.accessToken,
  });

  int idUser;
  String firstName;
  dynamic lastName;
  String email;
  String userThumbnail;
  dynamic gender;
  dynamic phoneNumber;
  DateTime modifiedDate;
  String userType;
  String accessToken;

  factory User.fromJson(Map<String, dynamic> json) => User(
        idUser: json["idUser"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        email: json["email"],
        userThumbnail: json["userThumbnail"],
        gender: json["gender"],
        phoneNumber: json["phoneNumber"],
        modifiedDate: DateTime.parse(json["modifiedDate"]),
        userType: json["userType"],
        accessToken: json["access_token"],
      );

  Map<String, dynamic> toJson() => {
        "idUser": idUser,
        "firstName": firstName,
        "lastName": lastName,
        "email": email,
        "userThumbnail": userThumbnail,
        "gender": gender,
        "phoneNumber": phoneNumber,
        "modifiedDate": modifiedDate.toIso8601String(),
        "userType": userType,
        "access_token": accessToken,
      };
}
