part of 'cart_bloc.dart';

@immutable
abstract class CartState {
  final List<Cart> carts;
  CartState({this.carts});

  int get totalCart => carts?.length;
  int get totalProduct {
    if (carts == null || carts.length == 0) {
      return 0;
    }
    var total = 0;
    for (var i = 0; i < carts.length; i++) {
      total += carts[i].quantity;
    }
    return total;
  }

  double get totalPrice {
    if (carts.length == 0) {
      return 0;
    }
    var total = 0.0;
    for (var i = 0; i < carts.length; i++) {
      total += carts[i].priceProduct * carts[i].quantity;
    }
    return total;
  }
}

class CartInitial extends CartState {}

class CartLoading extends CartState {
  CartLoading(List<Cart> carts) : super(carts: carts);
}

class CartSuccess extends CartState {
  CartSuccess(List<Cart> carts) : super(carts: carts);
}
