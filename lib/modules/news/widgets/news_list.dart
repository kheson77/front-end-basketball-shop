import 'package:basketballShop/Core/routes.dart';
import 'package:basketballShop/modules/news/bloc/news_bloc.dart';
import 'package:basketballShop/modules/news/models/newsModel.dart';
import 'package:basketballShop/widget/loading_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class NewsList extends StatefulWidget {
  @override
  _NewsListState createState() => _NewsListState();
}

class _NewsListState extends State<NewsList> {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Expanded(
      // padding: EdgeInsets.all(10),
      child: Container(
        padding: EdgeInsets.all(5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BlocBuilder<NewsBloc, NewsState>(
              builder: (context, state) {
                if (state is NewsInitial) {
                  return LoadingList();
                }
                return Expanded(
                  child: ListView.builder(
                      // padding: EdgeInsets.only(top: 10),
                      padding: EdgeInsets.zero,
                      itemCount: state.news.length,
                      itemBuilder: (context, i) {
                        if (i == 0) {
                          return InkWell(
                            onTap: () {
                              BlocProvider.of<NewsBloc>(context)
                                  .add(RelatedNewsEvent(i));
                              CoreRoutes.instance.navigateTo(
                                  CoreRouteNames.NEWS_DETAIL,
                                  arguments: state.news[i]);
                            },
                            child: Card(
                              elevation: 2,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Column(
                                children: [
                                  Hero(
                                    tag: state.news[i].idNews,
                                    child: Container(
                                      padding: EdgeInsets.zero,
                                      width: width,
                                      height: height / 3.5,
                                      child: Image.network(
                                        state.news[i].thumbnail,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: width,
                                    padding: EdgeInsets.only(
                                        left: 10, right: 10, bottom: 10),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "${state.news[i].title}",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16),
                                        ),
                                        SmoothStarRating(
                                          rating: 4.5,
                                          size: 15,
                                          color: Colors.amber,
                                          borderColor: Colors.amber,
                                          starCount: 5,
                                        ),
                                        Text(
                                          "${state.news[i].shortContent}",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        Text(
                                          "${state.news[i].content}",
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        Text(
                                          "Ngày chỉnh sửa: ${state.news[i].dateModified.toString().substring(1, 10)}",
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        }
                        return Container(
                          padding: EdgeInsets.only(left: 5, right: 5),
                          child: Card(
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  bottomLeft: Radius.circular(15)),
                            ),
                            child: InkWell(
                              // padding: EdgeInsets.only(bottom: 10),
                              borderRadius: BorderRadius.circular(8),
                              splashColor: Colors.orange,
                              onTap: () {
                                print(state.news[i].idNews);
                                BlocProvider.of<NewsBloc>(context).add(
                                    RelatedNewsEvent(state.news[i].idNews));
                                CoreRoutes.instance.navigateTo(
                                    CoreRouteNames.NEWS_DETAIL,
                                    arguments: state.news[i]);
                              },
                              child: Row(
                                children: [
                                  Hero(
                                    tag: state.news[i].idNews,
                                    child: Container(
                                      width: width / 4,
                                      height: width / 3.5,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(15),
                                              bottomLeft: Radius.circular(15)),
                                          image: DecorationImage(
                                              image: NetworkImage(
                                                  state.news[i].thumbnail),
                                              fit: BoxFit.cover)),
                                    ),
                                  ),
                                  Container(
                                    width: width * 3 / 4 - 30,
                                    padding: EdgeInsets.only(left: 10),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "${state.news[i].title}",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16),
                                        ),
                                        SmoothStarRating(
                                          rating: 4.5,
                                          size: 15,
                                          color: Colors.amber,
                                          borderColor: Colors.amber,
                                          starCount: 5,
                                        ),
                                        Text(
                                          "${state.news[i].shortContent}",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        Text(
                                          "${state.news[i].content}",
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        Text(
                                          "Ngày chỉnh sửa: ${state.news[i].dateModified.toString().substring(1, 10)}",
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
