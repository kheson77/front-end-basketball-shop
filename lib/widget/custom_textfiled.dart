import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField.edit(
      {Key key,
      this.hintText,
      @required this.keyboardType,
      @required this.txtController,
      @required this.onChanged})
      : this.validate = null,
        super(key: key);

  const CustomTextField.add(
      {Key key,
      this.hintText,
      @required this.validate,
      @required this.keyboardType,
      @required this.txtController,
      @required this.onChanged})
      : super(key: key);
  final TextEditingController txtController;
  final String hintText;
  final TextInputType keyboardType;
  final Function onChanged;
  final Function validate;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: validate == null ? 40 : 70,
        child: TextFormField(
            maxLines: 10,
            textAlign: TextAlign.left,
            autocorrect: false,
            obscureText: false,
            validator: validate ?? null,
            controller: txtController,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(10),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                  color: Colors.black,
                  width: 1,
                )),
                border: OutlineInputBorder(
                    borderSide: BorderSide(
                  color: Colors.black,
                  width: 1,
                )),
                // contentPadding: EdgeInsets.all(10.0),
                fillColor: Colors.white,
                floatingLabelBehavior: FloatingLabelBehavior.auto,
                filled: true,
                hintText: hintText ?? null,
                hintStyle: TextStyle(
                    color: Color(0xff969696), fontWeight: FontWeight.w300)),
            keyboardType: keyboardType,
            onChanged: this.onChanged));
  }
}
