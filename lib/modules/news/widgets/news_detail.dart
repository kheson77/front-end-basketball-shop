import 'package:basketballShop/Core/routes.dart';
import 'package:basketballShop/const/color_custom.dart';
import 'package:basketballShop/modules/news/bloc/news_bloc.dart';
import 'package:basketballShop/modules/news/models/newsModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NewsDetail extends StatelessWidget {
  NewsDetail({@required this.news});
  News news;
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(news.title),
        backgroundColor: Colors.white,
        actions: [
          IconButton(
            icon: Icon(Icons.more_vert),
            tooltip: 'Open shopping cart',
            onPressed: () {
              // handle the press
            },
            color: Colors.black,
          ),
        ],
        leading: IconButton(
          icon: Icon(Icons.arrow_back_outlined),
          color: Colors.black,
          onPressed: () {
            // Get.back();
            CoreRoutes.instance.pop();
          },
        ),
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                    padding:
                        const EdgeInsets.only(top: 12, left: 12, right: 12),
                    child: Text(
                      news.title,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                      textDirection: TextDirection.ltr,
                    )),
                Padding(
                    padding:
                        const EdgeInsets.only(top: 10, left: 12, right: 12),
                    child: Text(
                      news.shortContent,
                      style:
                          TextStyle(fontSize: 16, fontStyle: FontStyle.italic),
                      textDirection: TextDirection.ltr,
                    )),
                Hero(
                  tag: news.idNews,
                  child: Container(
                    padding: EdgeInsets.only(top: 12, left: 12, right: 12),
                    child: Container(
                        width: width,
                        height: height / 3.5,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(news.thumbnail)),
                          borderRadius: BorderRadius.all(Radius.circular(8.0)),
                          color: primaryColor,
                        )),
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.only(
                      left: 6,
                      right: 6,
                    ),
                    child: Text(
                      news.content,
                      textAlign: TextAlign.justify,
                    )),
                Container(
                  padding: const EdgeInsets.only(left: 12, right: 12),
                  child: Divider(
                    height: 12,
                    thickness: 1,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 12, right: 12, top: 8),
                  child: Text(
                    "BÀI VIẾT LIÊN QUAN",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                BlocBuilder<NewsBloc, NewsState>(
                  builder: (context, state) {
                    return SizedBox(
                      height: height / 4,
                      child: ListView.builder(
                        padding: EdgeInsets.only(
                            top: 10, left: 8, right: 8, bottom: 16),
                        scrollDirection: Axis.horizontal,
                        itemCount: state.relatedNews.length,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              BlocProvider.of<NewsBloc>(context).add(
                                  RelatedNewsEvent(state.news[index].idNews));
                              CoreRoutes.instance.navigateAndReplace(
                                  CoreRouteNames.NEWS_DETAIL,
                                  arguments: state.relatedNews[index]);
                            },
                            child: Container(
                              padding: EdgeInsets.only(
                                  top: 10, left: 8, right: 8, bottom: 16),
                              child: Column(
                                children: [
                                  Container(
                                    height: height / 8,
                                    width: width / 3,
                                    child: Container(
                                        decoration: BoxDecoration(
                                      image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: NetworkImage(state
                                              .relatedNews[index].thumbnail)),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(8.0)),
                                    )),
                                  ),
                                  Container(
                                    // height: Get.height / 12,
                                    padding: EdgeInsets.only(top: 4),
                                    width: width / 3,
                                    child: Text(
                                      state.relatedNews[index].title,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  },
                )
              ],
            )),
      ),
    );
  }
}
