import 'dart:async';

import 'package:basketballShop/modules/login/bloc/login_bloc.dart';
// import 'package:basketballShop/modules/login/model/user.dart';
import 'package:basketballShop/modules/manage/model/account.dart';
import 'package:basketballShop/modules/manage/repository/manage_repository.dart';
import 'package:basketballShop/modules/manage/widget/list/account/update_account.dart';
import 'package:basketballShop/modules/news/models/newsModel.dart';
import 'package:basketballShop/modules/product/models/product.dart';
import 'package:basketballShop/modules/shopping_cart/models/cart.dart';
import 'package:basketballShop/modules/userInfo/models/user.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'manage_event.dart';
part 'manage_state.dart';

class ManageBloc extends Bloc<ManageEvent, ManageState> {
  // ManageBloc() : super(ManageInitial());
  final LoginBloc loginBloc;
  StreamSubscription loginSubscription;
  ManageBloc({@required this.loginBloc}) : super(ManageInitial()) {
    void onLoginStateChanged(state) {
      if (state is LoginSuccessful) {
        print('loginSuccessful');
        if (state.user.userType == "manager") {
          add(GetAllManageEvent());
          accessToken = state.user.accessToken;
        }
      } else if (state is LoginFailed) {
        add(FirstLoadManageEvent());
      }
    }

    onLoginStateChanged(loginBloc.state);
    loginSubscription = loginBloc.stream.listen(onLoginStateChanged);
  }
  String accessToken;
  @override
  Stream<ManageState> mapEventToState(
    ManageEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is FirstLoadManageEvent) {
      // print('firstload');
      yield ManageInitial();
    } else if (event is GetAllManageEvent) {
      // print("event is GetAllManageEvent");
      yield ManageInitial();
      yield* _mapGetAllManageEventToState(event);
    } else if (event is DeleteProductEvent) {
      yield ManageLoad(state.accounts, state.products, state.carts, state.news);
      yield* _mapDeleteProductEventToState(event);
    } else if (event is UpdateProductEvent) {
      yield ManageLoad(state.accounts, state.products, state.carts, state.news);
      yield* _mapUpdateProductEventToState(event);
    } else if (event is AddProductEvent) {
      yield ManageLoad(state.accounts, state.products, state.carts, state.news);
      yield* _mapAddProductEventToState(event);
    } else if (event is UpdateAccountEvent) {
      yield ManageLoad(state.accounts, state.products, state.carts, state.news);
      yield* _mapUpdateAccountEventToState(event);
    } else if (event is DeleteAccountEvent) {
      yield ManageLoad(state.accounts, state.products, state.carts, state.news);
      yield* _mapDeleteAccountEventToState(event);
    }
  }

  Stream<ManageState> _mapGetAllManageEventToState(
      GetAllManageEvent event) async* {
    try {
      // await Future.delayed(Duration(milliseconds: 1500));
      var getAccount = await ManageRepository().getAccounts(accessToken);
      var getProduct = await ManageRepository().getProduct(accessToken);
      var getCart = await ManageRepository().getCart(accessToken);
      var getNews = await ManageRepository().getNews();
      yield ManageSuccess(getAccount.results, getProduct.results,
          getCart.results, getNews.results);
    } catch (_) {}
  }

  Stream<ManageState> _mapDeleteProductEventToState(
      DeleteProductEvent event) async* {
    try {
      await ManageRepository().deleteProduct(accessToken, event.idProduct);
      var getProduct = await ManageRepository().getProduct(accessToken);
      yield ManageSuccess(
          state.accounts, getProduct.results, state.carts, state.news);
    } catch (_) {}
  }

  Stream<ManageState> _mapUpdateProductEventToState(
      UpdateProductEvent event) async* {
    try {
      await ManageRepository()
          .updateProduct(accessToken, event.product, event.upLoadImage);
      var getProduct = await ManageRepository().getProduct(accessToken);
      yield ManageSuccess(
          state.accounts, getProduct.results, state.carts, state.news);
    } catch (_) {}
  }

  Stream<ManageState> _mapAddProductEventToState(AddProductEvent event) async* {
    try {
      await ManageRepository().addProduct(accessToken, event.product);
      var getProduct = await ManageRepository().getProduct(accessToken);
      yield ManageSuccess(
          state.accounts, getProduct.results, state.carts, state.news);
    } catch (_) {}
  }

  Stream<ManageState> _mapUpdateAccountEventToState(
      UpdateAccountEvent event) async* {
    try {
      print('update account');
      await ManageRepository()
          .updateAccount(event.account.idUser, accessToken, event.account);
      var getUser = await ManageRepository().getAccounts(accessToken);
      yield ManageSuccess(
          getUser.results, state.products, state.carts, state.news);
    } catch (_) {}
  }

  Stream<ManageState> _mapDeleteAccountEventToState(
      DeleteAccountEvent event) async* {
    try {
      await ManageRepository().deleteUser(accessToken, event.idAccount);
      var getUser = await ManageRepository().getAccounts(accessToken);
      yield ManageSuccess(
          getUser.results, state.products, state.carts, state.news);
    } catch (_) {}
  }
}
