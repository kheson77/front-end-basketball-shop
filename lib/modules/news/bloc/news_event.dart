part of 'news_bloc.dart';

@immutable
abstract class NewsEvent {}

class FirstLoadNews extends NewsEvent {}

class RelatedNewsEvent extends NewsEvent {
  final int idNews;
  RelatedNewsEvent(this.idNews);
}
