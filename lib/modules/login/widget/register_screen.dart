import 'package:basketballShop/Core/routes.dart';
import 'package:basketballShop/modules/login/repository/login_repository.dart';
import 'package:basketballShop/widget/buildShowDialog.dart';
import 'package:basketballShop/widget/custom_button.dart';
import 'package:basketballShop/widget/login/input_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:toast/toast.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final TextEditingController name = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController pass = TextEditingController();
  final TextEditingController confirmPass = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _validateName =
      MultiValidator([RequiredValidator(errorText: 'Tên không được để trống')]);
  final _validateEmail = MultiValidator([
    EmailValidator(
      errorText: 'Email không hợp lệ',
    ),
    RequiredValidator(errorText: 'Email không được để trống')
  ]);
  final _validatePass = MultiValidator([
    RequiredValidator(errorText: 'Password không được để trống'),
    MinLengthValidator(6, errorText: 'Nhập ít nhất 6 ký tự')
  ]);
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        body: Builder(
          builder: (context) => SingleChildScrollView(
            child: Stack(
              children: [
                // Positioned(
                //   bottom: 0,
                //   width: width * 1.2,
                //   child: SvgPicture.asset('assets/design.svg'),
                // ),
                Positioned(
                    top: 0,
                    child: SafeArea(
                      child: Container(
                        height: 120,
                        width: width,
                        decoration: BoxDecoration(
                            // color: Colors.purple,
                            gradient: LinearGradient(
                                colors: [
                                  Colors.purpleAccent,
                                  Colors.deepPurple
                                ],
                                begin: const FractionalOffset(0.0, 0.0),
                                end: const FractionalOffset(0.5, 0.0),
                                stops: [0.1, 1.0],
                                tileMode: TileMode.clamp),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(30),
                                bottomRight: Radius.circular(30))),
                      ),
                    )),
                Container(
                  height: height,
                  width: width,
                  child: Form(
                    key: _formKey,
                    autovalidate: true,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: height * 0.08,
                        ),
                        Padding(
                            padding: EdgeInsets.only(left: 20, right: 20),
                            child: Row(
                              children: [
                                InkWell(
                                  onTap: () {
                                    CoreRoutes.instance.pop();
                                  },
                                  child: Icon(
                                    Icons.chevron_left,
                                    size: 35,
                                    color: Colors.white,
                                  ),
                                ),
                                // SizedBox(
                                //   width: 10,
                                // ),
                                Spacer(),
                                Text(
                                  "Đăng ký",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 28,
                                      color: Colors.white),
                                ),
                                Spacer(),
                                Container(
                                  width: 35,
                                )
                              ],
                            )),
                        Container(
                          alignment: Alignment.center,
                          padding:
                              EdgeInsets.only(top: 15, left: 20, right: 20),
                          child: Text(
                            "Đăng ký ngay để có thể mua sắm nào",
                            style:
                                TextStyle(fontSize: 15.5, color: Colors.white),
                          ),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        InputText.name(
                            hint_text: "Tên của bạn",
                            label_text: "Tên của bạn",
                            textEditingController: name,
                            validator: _validateName),
                        InputText.email(
                            hint_text: "Email",
                            label_text: "Email",
                            textEditingController: email,
                            validator: _validateEmail),
                        InputText.password(
                            hint_text: "Mật khẩu",
                            label_text: "Mật khẩu",
                            textEditingController: pass,
                            validator: _validatePass),
                        InputText.password(
                            hint_text: "Nhập lại mật khẩu",
                            label_text: "Nhập lại mật khẩu",
                            textEditingController: confirmPass,
                            validator: _validatePass),
                        Container(
                          padding: EdgeInsets.only(top: 30),
                          child: CustomButton("Đăng ký", () {
                            if (_formKey.currentState.validate()) {
                              if (pass.text != confirmPass.text) {
                                Scaffold.of(context).showSnackBar(SnackBar(
                                    content: Text(
                                        "Mật khẩu xác nhận không khớp với mật khẩu")));
                              } else {
                                final String nameUser = name.text;
                                final String emailUser = email.text;
                                final String password = pass.text;
                                LoginRepository()
                                    .register(nameUser, emailUser, password)
                                    .then((value) async {
                                  buildShowDialog(context);
                                  await Future.delayed(Duration(seconds: 1))
                                      .then((value) {
                                    Navigator.of(context).pop();
                                  }).then((value) {
                                    Toast.show("Đăng ký thành công!", context,
                                        duration: Toast.LENGTH_SHORT,
                                        gravity: Toast.BOTTOM);
                                    CoreRoutes.instance.pop();
                                  });
                                });
                              }
                            } else {
                              Scaffold.of(context).showSnackBar(SnackBar(
                                  content: Text(
                                      "Vui lòng điền đúng thông tin đăng ký!")));
                            }
                          }, Colors.purple),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
