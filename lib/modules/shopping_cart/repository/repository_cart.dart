import 'dart:convert';

import 'package:basketballShop/const/baseUrl.dart';
import 'package:basketballShop/modules/shopping_cart/models/cart.dart';
import 'package:basketballShop/modules/product/models/product.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;

class RepositoryCart {
  Future<CartModel> getCart(int userId, String accessToken) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = accessToken;
    Response response = await dio.get(API_GET_CART_BY_ID + "$userId");
    if (response.statusCode == 200) {
      // print(response.data);
      return CartModel.fromJson(response.data);
    } else
      return null;
  }

  Future<void> addCart(
      Product product, String accessToken, int idUser, int count) async {
    final encoding = Encoding.getByName('utf-8');
    Map body = {
      "idUser": idUser,
      "idProduct": product.idProduct,
      "idCategory": product.idCategory,
      "nameProduct": product.name,
      "thumbnailProduct": product.thumbnail,
      "priceProduct": product.price,
      "quantity": count
    };
    // print(body);
    var response = await http.post(Uri.parse(API_POST_CART),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': accessToken
        },
        body: json.encode(body),
        encoding: encoding);
    print(response.statusCode);
    if (response.statusCode == 200) {
      // print(response.data);
      print('ok');
    } else
      return null;
  }

  Future<void> deleteCart(int id, String accessToken) async {
    print('${API_POST_CART}$id');
    var response = await http.delete(
      Uri.parse('${API_POST_CART}$id'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': accessToken
      },
    );
    print(response.statusCode);
    if (response.statusCode == 200) {
      print(response.body);
      print('ok');
    } else
      return null;
  }

  Future<void> updateCart(int idCart, String accessToken, int count) async {
    final encoding = Encoding.getByName('utf-8');
    Map body = {"quantity": count};
    var response = await http.patch(Uri.parse('${API_POST_CART}$idCart'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': accessToken
        },
        body: json.encode(body),
        encoding: encoding);
    if (response.statusCode == 200) {
      // print(response.data);
      print('ok');
    } else
      return null;
  }
}
