import 'dart:io';

import 'package:basketballShop/modules/manage/bloc/manage_bloc.dart';
import 'package:basketballShop/modules/product/models/product.dart';
import 'package:basketballShop/widget/custom_button.dart';
import 'package:basketballShop/widget/custom_textfiled.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:toast/toast.dart';

class AddProduct extends StatefulWidget {
  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  TextEditingController _nameProduct = TextEditingController();
  TextEditingController _priceProduct = TextEditingController();
  TextEditingController _countProduct = TextEditingController();
  TextEditingController _descriptionProduct = TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  int selectCate;
  String selectStatus;

  File _image;
  final picker = ImagePicker();

  Future getImageFromCamera() async {
    final pickedImage =
        await picker.getImage(source: ImageSource.camera, imageQuality: 100);

    setState(() {
      if (pickedImage != null) {
        _image = File(pickedImage.path);
      } else {
        print("No Image Selected");
      }
    });
  }

  Future getImageFromGallery() async {
    final pickedImage =
        await picker.getImage(source: ImageSource.gallery, imageQuality: 100);

    setState(() {
      if (pickedImage != null) {
        _image = File(pickedImage.path);
      } else {
        print("No Image Selected");
      }
    });
  }

  final _validate =
      MultiValidator([RequiredValidator(errorText: 'Không được để trống')]);

  @override
  void initState() {
    super.initState();
    selectCate = 5;
    selectStatus = 'active';
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Thêm sản phẩm",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        actions: [
          IconButton(
            icon: Icon(Icons.more_vert),
            onPressed: () {
              showDialog(context: context, builder: (_) {});
            },
            color: Colors.black,
          ),
        ],
        leading: IconButton(
          icon: Icon(Icons.arrow_back_outlined),
          color: Colors.black,
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        elevation: 0,
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Form(
              key: _formKey,
              autovalidate: true,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: width,
                    margin: EdgeInsets.symmetric(vertical: 10),
                    alignment: Alignment.center,
                    child: Container(
                      width: 150,
                      height: 150,
                      alignment: Alignment.bottomRight,
                      child: Container(
                          padding: EdgeInsets.only(right: 20),
                          child: Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle, color: Colors.white),
                              child: GestureDetector(
                                  onTap: () {
                                    showModalBottomSheet(
                                      context: context,
                                      builder: ((builder) => _bottomSheet()),
                                    );
                                  },
                                  child: Icon(Icons.camera_alt)))),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: _image != null
                                  ? FileImage(_image)
                                  : AssetImage('assets/default_image.jpg'),
                              fit: BoxFit.cover)),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                    child: Text("Tên sản phẩm"),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: CustomTextField.add(
                      validate: _validate,
                      txtController: _nameProduct,
                      keyboardType: TextInputType.text,
                      onChanged: (String value) {
                        // check();
                      },
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
                    child: Text("Trạng thái"),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 20),
                    child: Container(
                      width: width - 40,
                      height: 40,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        border: Border.all(
                            color: Colors.black,
                            style: BorderStyle.solid,
                            width: 0.7),
                      ),
                      child: Container(
                        margin: EdgeInsets.only(left: 10, right: 10),
                        child: DropdownButton(
                            // elevation: 0,
                            isExpanded: true,
                            value: selectStatus,
                            icon: Icon(
                              Icons.arrow_drop_down,
                              size: 25,
                            ),
                            underline: SizedBox(),
                            items: ['active', 'inactive']
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(getStatus(value)),
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                selectStatus = value;
                              });
                            }),
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
                    child: Text("Loại sản phẩm"),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 20),
                    child: Container(
                      width: width - 40,
                      height: 40,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        border: Border.all(
                            color: Colors.black,
                            style: BorderStyle.solid,
                            width: 0.7),
                      ),
                      child: Container(
                        margin: EdgeInsets.only(left: 10, right: 10),
                        child: DropdownButton(
                            // elevation: 0,
                            isExpanded: true,
                            value: selectCate,
                            icon: Icon(
                              Icons.arrow_drop_down,
                              size: 25,
                            ),
                            underline: SizedBox(),
                            items: [5, 6, 7, 8]
                                .map<DropdownMenuItem<int>>((int value) {
                              return DropdownMenuItem<int>(
                                value: value,
                                child: Text(getCategory(value)),
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                selectCate = value;
                              });
                              // print(selectCate);
                            }),
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
                    child: Text("Số lượng"),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: CustomTextField.add(
                      validate: _validate,
                      txtController: _countProduct,
                      keyboardType: TextInputType.text,
                      onChanged: (String value) {
                        // check();
                      },
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
                    child: Text("Giá sản phẩm"),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: CustomTextField.add(
                      validate: _validate,
                      txtController: _priceProduct,
                      keyboardType: TextInputType.text,
                      onChanged: (String value) {
                        // check();
                      },
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
                    child: Text("Mô tả"),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    height: 160,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: CustomTextField.add(
                      validate: _validate,
                      txtController: _descriptionProduct,
                      keyboardType: TextInputType.text,
                      onChanged: (String value) {
                        // check();
                      },
                    ),
                  ),
                  SizedBox(height: 120),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 20),
              color: Colors.grey.withOpacity(0.8),
              child: CustomButton('Thêm sản phẩm', () {
                if (!_formKey.currentState.validate() || _image == null) {
                  print(context);
                  _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content: Text("Vui lòng không để trống các mục!")));
                } else {
                  BlocProvider.of<ManageBloc>(context)
                      .add(AddProductEvent(dataProduct()));
                  Navigator.of(context).pop();
                  Toast.show("Thêm sản phẩm thành công!", context,
                      duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                }
              }, Colors.blue),
            ),
          ),
        ],
      ),
    );
  }

  Product dataProduct() {
    final String name = _nameProduct.text;
    final int idCategory = selectCate;
    final String status = selectStatus;
    final int count = int.parse(_countProduct.text);
    final double price = double.parse(_priceProduct.text);
    final String desciption = _descriptionProduct.text;
    final String thumbnail = _image.path;

    Product product = Product(
        name: name,
        status: status,
        count: count,
        price: price,
        description: desciption,
        thumbnail: thumbnail,
        idCategory: idCategory);
    return product;
  }

  String getCategory(int i) {
    if (i == 5) {
      return "Giày bóng rổ";
    } else if (i == 6) {
      return "Quần áo bóng rổ";
    } else if (i == 7) {
      return "Phụ kiện bóng rổ";
    } else if (i == 8) {
      return "Quả bóng rổ";
    }
  }

  String getStatus(String s) {
    if (s == 'active') {
      return 'Hoạt động';
    } else if (s == 'inactive') {
      return 'Không hoạt động';
    }
  }

  Widget _bottomSheet() {
    return Container(
      height: 100,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Column(
        children: [
          Text(
            "Choose Profile Photo",
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              FlatButton.icon(
                onPressed: getImageFromCamera,
                icon: Icon(Icons.camera),
                label: Text("Camera"),
              ),
              FlatButton.icon(
                onPressed: getImageFromGallery,
                icon: Icon(Icons.image),
                label: Text("Galery"),
              )
            ],
          )
        ],
      ),
    );
  }
}
