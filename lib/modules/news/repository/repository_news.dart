import 'package:basketballShop/const/baseUrl.dart';
import 'package:basketballShop/modules/news/models/newsModel.dart';
import 'package:dio/dio.dart';

class RepositoryNews {
  Future<NewsModel> getNews() async {
    // await Future.delayed(Duration(seconds: 2));
    Response response = await Dio().get(API_GET_NEWS);
    if (response.statusCode == 200) {
      // print(response.data);
      return NewsModel.fromJson(response.data);

      // Tea tea = Tea.fromJson(response.data);
    } else
      return null;
  }

  Future<NewsModel> getRelatedNews(int idNews) async {
    Response response = await Dio().get("${API_GET_RELATED_NEWS}${idNews}");
    if (response.statusCode == 200) {
      // print(response.data);
      return NewsModel.fromJson(response.data);

      // Tea tea = Tea.fromJson(response.data);
    } else
      return null;
  }
}
