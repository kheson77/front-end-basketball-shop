part of 'news_bloc.dart';

@immutable
abstract class NewsState {
  List<News> news;
  List<News> relatedNews;
  NewsState({this.news, this.relatedNews});
}

class NewsInitial extends NewsState {}

class NewsSuccessLoaded extends NewsState {
  NewsSuccessLoaded({List<News> news, List<News> relatedNews})
      : super(news: news, relatedNews: relatedNews);
}
