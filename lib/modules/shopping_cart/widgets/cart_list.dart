import 'package:basketballShop/modules/shopping_cart/bloc/cart_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:toast/toast.dart';

class CartList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    final CartBloc bloc = BlocProvider.of<CartBloc>(context);
    return BlocBuilder<CartBloc, CartState>(
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: height * 0.05,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Text(
                      "Giỏ hàng",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 28,
                          color: Colors.black),
                    )),
                Container(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Icon(Icons.search)),
              ],
            ),
            Container(
              height: 430,
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemBuilder: (context, index) {
                  final cart = state.carts[index];
                  return Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                      elevation: 5,
                      child: Container(
                        margin: EdgeInsets.all(10),
                        child: Row(
                          // crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: 100,
                              width: width / 4,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  image: DecorationImage(
                                      image: NetworkImage(
                                          '${cart.thumbnailProduct}'),
                                      fit: BoxFit.cover)),
                            ),
                            Container(
                              width: width / 2.5,
                              height: 100,
                              padding: EdgeInsets.only(left: 20),
                              child: Column(
                                // mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    '${cart.nameProduct}',
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  Text("${cart.priceProduct} đ",
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.blue)),
                                  Text("Màu: đỏ"),
                                  Spacer(),
                                  Container(
                                    margin: EdgeInsets.only(right: 10),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        border: Border.all(
                                            color: Colors.grey, width: 2)),
                                    child: Container(
                                      height: 35,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          InkWell(
                                            child: SvgPicture.asset(
                                              'assets/minus.svg',
                                              height: 25,
                                              width: 25,
                                              color: cart.quantity == 1
                                                  ? Colors.grey
                                                  : Colors.black,
                                            ),
                                            onTap: cart.quantity == 1
                                                ? () {}
                                                : () {
                                                    bloc.add(DecreaseItemEvent(
                                                        cart));
                                                  },
                                          ),
                                          Text("${cart.quantity}"),
                                          InkWell(
                                            child: SvgPicture.asset(
                                              'assets/plus.svg',
                                              height: 25,
                                              width: 25,
                                            ),
                                            onTap: () {
                                              // final bloc = locator<ShoppingcartBloc>();
                                              bloc.add(IncreaseItemEvent(cart));
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Spacer(),
                            Container(
                              height: 100,
                              alignment: Alignment.topRight,
                              child: InkWell(
                                  onTap: () {
                                    Toast.show(
                                        "Xóa thành công sản phẩm: ${cart.nameProduct}",
                                        context,
                                        duration: Toast.LENGTH_SHORT,
                                        gravity: Toast.BOTTOM);
                                    bloc.add(DeleteItemEvent(cart.idCart));
                                  },
                                  child: Icon(Icons.close)),
                            )
                          ],
                        ),
                      ));
                },
                itemCount: state.carts.length,
              ),
            ),
          ],
        );
      },
    );
  }
}
