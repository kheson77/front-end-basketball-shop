import 'package:basketballShop/Core/routes.dart';
import 'package:basketballShop/modules/login/widget/register_screen.dart';
import 'package:basketballShop/modules/manage/widget/list/account/manage_account_tab.dart';
import 'package:basketballShop/modules/manage/widget/list/account/update_account.dart';
import 'package:basketballShop/modules/manage/widget/list/news/add_news.dart';
import 'package:basketballShop/modules/manage/widget/list/news/manage_news_list.dart';
import 'package:basketballShop/modules/manage/widget/list/news/update_news.dart';
import 'package:basketballShop/modules/manage/widget/list/product/add_product.dart';
import 'package:basketballShop/modules/manage/widget/list/product/update_product.dart';
import 'package:basketballShop/modules/manage/widget/list/product/manage_product_tab.dart';
import 'package:basketballShop/modules/news/widgets/news_detail.dart';
import 'package:basketballShop/navigation_menu.dart';
import 'package:basketballShop/modules/product/widgets/product_detail.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'modules/userInfo/widgets/dashboard/info_user_screen.dart';

class Routes extends CoreRoutes {
  factory Routes() => CoreRoutes.instance;

  static Route<dynamic> generateRoute(RouteSettings settings) {
    print(settings.name);
    switch (settings.name) {
      case CoreRouteNames.HOME_SCREEN:
        return MaterialPageRoute(builder: (context) => NavigationMenu());
      case CoreRouteNames.PRODUCT_DETAIL:
        var product = settings.arguments;
        return MaterialPageRoute(
            builder: (context) => ProductDetail(
                  product: product,
                ));
      case CoreRouteNames.NEWS_DETAIL:
        var news = settings.arguments;
        return MaterialPageRoute(
            builder: (context) => NewsDetail(
                  news: news,
                ));
      case CoreRouteNames.REGISTER_SCREEN:
        return MaterialPageRoute(builder: (context) => RegisterScreen());
      case CoreRouteNames.USER_INFO_SCREEN:
        return MaterialPageRoute(builder: (context) => InfoUserScreen());
      case CoreRouteNames.MANAGE_ACCOUNT_TAB:
        return MaterialPageRoute(builder: (context) => ManageAccountTab());
      case CoreRouteNames.MANAGE_PRODUCT_TAB:
        return MaterialPageRoute(builder: (context) => ManageProductTab());
      case CoreRouteNames.MANAGE_NEWS_LIST:
        return MaterialPageRoute(builder: (context) => ManageNewsList());
      case CoreRouteNames.UPDATE_PRODUCT:
        var product = settings.arguments;
        return MaterialPageRoute(
            builder: (context) => UpdateProduct(
                  product: product,
                ));
      case CoreRouteNames.ADD_PRODUCT:
        return MaterialPageRoute(builder: (context) => AddProduct());
      case CoreRouteNames.UPDATE_ACCOUNT:
        var user = settings.arguments;
        return MaterialPageRoute(
            builder: (context) => UpdateAccount(
                  user: user,
                ));
      case CoreRouteNames.ADD_NEWS:
        return MaterialPageRoute(builder: (context) => AddNews());
      case CoreRouteNames.UPDATE_NEWS:
        var news = settings.arguments;
        return MaterialPageRoute(builder: (context) => UpdateNews(news: news));
    }
  }
}
