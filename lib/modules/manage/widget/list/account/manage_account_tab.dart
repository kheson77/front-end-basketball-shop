import 'package:basketballShop/Core/routes.dart';
import 'package:basketballShop/modules/manage/bloc/manage_bloc.dart';
import 'package:basketballShop/modules/manage/widget/list/account/manage_account_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ManageAccountTab extends StatefulWidget {
  @override
  _ManageAccountTabState createState() => _ManageAccountTabState();
}

class _ManageAccountTabState extends State<ManageAccountTab> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Tài khoản",
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
          actions: [
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {},
              color: Colors.black,
            ),
          ],
          leading: IconButton(
            icon: Icon(Icons.arrow_back_outlined),
            color: Colors.black,
            onPressed: () {
              CoreRoutes.instance.pop();
            },
          ),
          elevation: 0,
          bottom: const TabBar(
            labelColor: Colors.black,
            unselectedLabelColor: Colors.grey,
            isScrollable: true,
            tabs: <Widget>[
              Tab(
                text: "Người dùng",
              ),
              Tab(
                text: "Quản trị viên",
              ),
            ],
          ),
        ),
        body: BlocBuilder<ManageBloc, ManageState>(
          builder: (context, state) {
            return TabBarView(
              children: [
                ManageAccountList(
                  accounts: state.customers,
                ),
                ManageAccountList(
                  accounts: state.managers,
                )
              ],
            );
          },
        ),
      ),
    );
  }
}
