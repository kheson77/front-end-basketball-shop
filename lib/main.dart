import 'package:basketballShop/Core/routes.dart';
import 'package:basketballShop/modules/login/bloc/login_bloc.dart';
import 'package:basketballShop/modules/manage/bloc/manage_bloc.dart';
import 'package:basketballShop/modules/news/bloc/news_bloc.dart';
import 'package:basketballShop/modules/product/bloc/product_bloc.dart';
import 'package:basketballShop/modules/shopping_cart/bloc/cart_bloc.dart';
import 'package:basketballShop/modules/tabbar/category_tabbar_controller.dart';
import 'package:basketballShop/modules/tabbar/tabbar_controller.dart';
import 'package:basketballShop/modules/userInfo/bloc/userinfo_bloc.dart';
import 'package:basketballShop/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

void main() {
  runApp(BlocProvider(
    create: (context) => LoginBloc()..add(LoginFirstLoadEvent()),
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  TabbarController tabbarController = Get.put(TabbarController());
  CategoryTabbarController categoryTabbarController =
      Get.put(CategoryTabbarController());
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ProductBloc>(
          create: (BuildContext context) =>
              ProductBloc()..add(FirstLoadProductEvent()),
        ),
        BlocProvider<CartBloc>(
          create: (BuildContext context) =>
              CartBloc(loginBloc: BlocProvider.of<LoginBloc>(context)),
        ),
        BlocProvider<UserinfoBloc>(
            create: (BuildContext context) =>
                UserinfoBloc(loginBloc: BlocProvider.of<LoginBloc>(context))
            // ..add(UserInfoFirstLoadEvent()),
            ),
        BlocProvider<ManageBloc>(
          create: (BuildContext context) =>
              ManageBloc(loginBloc: BlocProvider.of<LoginBloc>(context)),
        ),
        BlocProvider<NewsBloc>(
            create: (BuildContext context) => NewsBloc()..add(FirstLoadNews())),
        // BlocProvider<LoginBloc>(
        //   create: (BuildContext context) =>
        //       LoginBloc()..add(LoginFirstLoadEvent()),
        // ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        navigatorKey: CoreRoutes.instance.navigatorKey,
        onGenerateRoute: Routes.generateRoute,
        initialRoute: CoreRouteNames.HOME_SCREEN,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        // home: MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}
