import 'dart:convert';

import 'package:basketballShop/const/baseUrl.dart';
import 'package:basketballShop/modules/manage/model/account.dart';
import 'package:basketballShop/modules/news/models/newsModel.dart';
import 'package:basketballShop/modules/product/models/product.dart';
import 'package:basketballShop/modules/shopping_cart/models/cart.dart';
import 'package:basketballShop/modules/userInfo/models/user.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';

class ManageRepository {
  // Get Account
  Future<AccountModel> getAccounts(String accessToken) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = accessToken;
    Response response = await dio.get(API_GET_USER);
    if (response.statusCode == 200) {
      return AccountModel.fromJson(response.data);
    } else {
      return null;
    }
  }

  // Get Product
  Future<ProductModel> getProduct(String accessToken) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = accessToken;
    Response response = await dio.get(API_GET_ALL_PRODUCT);
    if (response.statusCode == 200) {
      return ProductModel.fromJson(response.data);
    } else
      return null;
  }

  // Get News
  Future<NewsModel> getNews() async {
    Response response = await Dio().get(API_GET_NEWS);
    if (response.statusCode == 200) {
      return NewsModel.fromJson(response.data);
    } else
      return null;
  }

  // Get Shopping Cart
  Future<CartModel> getCart(String accessToken) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = accessToken;
    Response response = await dio.get(API_GET_CART);
    if (response.statusCode == 200) {
      return CartModel.fromJson(response.data);
    } else
      return null;
  }

  // Update Product
  Future<void> updateProduct(
      String accessToken, Product product, bool upLoadImage) async {
    final encoding = Encoding.getByName('utf-8');
    if (upLoadImage == false) {
      Map body = {
        "name": product.name,
        "price": product.price,
        "thumbnail": product.thumbnail,
        "description": product.description,
        "count": product.count,
        "status": product.status,
        "idCategory": product.idCategory
      };

      var response = await http.patch(
          Uri.parse('${API_UPDATE_PRODUCT}${product.idProduct}'),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': accessToken
          },
          body: json.encode(body),
          encoding: encoding);
      if (response.statusCode == 200) {
        print('update thanh cong');
      } else {
        return null;
      }
    } else {
      var thumbnail = await http.MultipartFile.fromPath(
          "thumbnail", product.thumbnail,
          contentType: MediaType("image", "png"));

      var request = http.MultipartRequest(
          "PATCH", Uri.parse('${API_UPDATE_PRODUCT}${product.idProduct}'));
      request.headers["Authorization"] = accessToken;
      request.headers["Content-Type"] = "multipart/form-data";
      request.fields["name"] = product.name;
      request.fields["price"] = product.price.toString();
      request.fields["description"] = product.description;
      request.fields["count"] = product.count.toString();
      request.fields["status"] = product.status;
      request.fields["idCategory"] = product.idCategory.toString();
      request.files.add(thumbnail);
      request.send().then((response) {
        print(response.statusCode);
        print("update thanh cong");
      }).catchError((error) => print(error));
    }
  }

  // Add product
  Future<void> addProduct(String accessToken, Product product) async {
    // final encoding = Encoding.getByName('utf-8');
    // Map body = {
    //   "name": product.name,
    //   "price": product.price,
    //   "thumbnail": product.thumbnail,
    //   "description": product.description,
    //   "idCategory": product.idCategory
    // };
    // var response = await http.post(Uri.parse('${API_GET_PRODUCT}'),
    //     headers: {
    //       'Content-Type': 'application/json',
    //       'Authorization': accessToken
    //     },
    //     body: json.encode(body),
    //     encoding: encoding);
    // if (response.statusCode == 200) {
    //   print('them thanh cong');
    // } else {
    //   return null;
    // }

    // print(product.thumbnail);

    // Dio dio = new Dio();
    // dio.options.headers['content-Type'] = "multipart/form-data";
    // dio.options.headers["Authorization"] = accessToken;

    // String fileName = product.thumbnail.split('/').last;
    // Map<String, dynamic> formData = {
    //   "name": product.name,
    //   "price": product.price,
    //   "thumbnail": await MultipartFile.fromFile(product.thumbnail,
    //       filename: fileName, contentType: MediaType("image", "png")),
    //   "description": product.description,
    //   "idCategory": product.idCategory
    // };

    // var response = await dio
    //     .post("http://10.0.2.2:3000/api/product", data: formData)
    //     .then((response) => print(response))
    //     .catchError((error) => print(error));
    // print(response.statusCode);
    // if (response.statusCode == 200) {
    //   print('them thanh cong');
    // } else {
    //   return null;
    // }
    var thumbnail = await http.MultipartFile.fromPath(
        "thumbnail", product.thumbnail,
        contentType: MediaType("image", "png"));

    var request = http.MultipartRequest("POST", Uri.parse(API_GET_PRODUCT));
    request.headers["Authorization"] = accessToken;
    request.headers["Content-Type"] = "multipart/form-data";
    request.fields["name"] = product.name;
    request.fields["price"] = product.price.toString();
    request.fields["description"] = product.description;
    request.fields["count"] = product.count.toString();
    request.fields["status"] = product.status;
    request.fields["idCategory"] = product.idCategory.toString();
    request.files.add(thumbnail);
    request.send().then((response) {
      print(response.statusCode);
      print("thêm thành công");
    }).catchError((error) => print(error));
  }

  // Delete product
  Future<void> deleteProduct(String accessToken, int idProduct) async {
    var response = await http.delete(
      Uri.parse('${API_GET_PRODUCT}/$idProduct'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': accessToken
      },
    );
    if (response.statusCode == 200) {
      print('Xoa thanh cong');
    } else
      return null;
  }

  Future<void> updateAccount(int id, String accessToken, User user) async {
    final encoding = Encoding.getByName('utf-8');

    Map body = {
      "firstName": user.firstName,
      "lastName": user.lastName,
      "gender": user.gender,
      "phoneNumber": user.phoneNumber,
      "userType": user.userType
    };
    print(body['lastName']);
    var res = await http.patch(Uri.parse('${API_UPDATE_INFO_USER}${id}'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': accessToken
        },
        body: json.encode(body),
        encoding: encoding);
    if (res.statusCode == 200) {
      print(res.body);
      print('Cap nhat user thanh cong');
    } else
      return null;
  }

  Future<void> deleteUser(String accessToken, int idUser) async {
    var response = await http.delete(
      Uri.parse('${API_DELETE_ACCOUNT}/$idUser'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': accessToken
      },
    );
    if (response.statusCode == 200) {
      print('Xoa thanh cong');
    } else
      return null;
  }
}
