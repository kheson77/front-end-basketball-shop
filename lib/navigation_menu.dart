import 'package:basketballShop/const/color_custom.dart';
import 'package:basketballShop/modules/login/bloc/login_bloc.dart';
import 'package:basketballShop/modules/login/widget/login_screen.dart';
import 'package:basketballShop/modules/shopping_cart/bloc/cart_bloc.dart';
import 'package:basketballShop/modules/tabbar/category_tabbar_controller.dart';
import 'package:basketballShop/modules/tabbar/tabbar_controller.dart';
import 'package:basketballShop/pages/customer_page/cart_screen.dart';
import 'package:basketballShop/pages/customer_page/home_screen.dart';
import 'package:basketballShop/pages/customer_page/news_screen.dart';
import 'package:basketballShop/pages/customer_page/product_page.dart';
import 'package:basketballShop/pages/manager_page/manager.dart';
import 'package:basketballShop/pages/manager_page/statistic.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class NavigationMenu extends StatefulWidget {
  @override
  _NavigationMenuState createState() => _NavigationMenuState();
}

class _NavigationMenuState extends State<NavigationMenu> {
  TabbarController tabbarController = Get.find();
  CategoryTabbarController categoryTabbarController = Get.find();

  static final List<dynamic> _widgetOptions = [
    HomeScreen(),
    ProductPage(),
    NewsScreen(),
    CartScreen(),
    LoginScreen(),
  ];

  static final List<dynamic> _widgetOptionsManager = [
    Manager(),
    Statistic(),
    LoginScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      builder: (context, state) {
        if (state.userType == "manager") {
          return Scaffold(
              body: Obx(() => Container(
                    child: _widgetOptionsManager
                        .elementAt(tabbarController.index.value),
                  )),
              bottomNavigationBar: Obx(
                () => SizedBox(
                  height: 80,
                  child: BottomNavigationBar(
                    showSelectedLabels: false,
                    showUnselectedLabels: false,
                    type: BottomNavigationBarType.fixed,
                    items: [
                      BottomNavigationBarItem(
                        icon: Column(
                          children: [
                            SvgPicture.asset(
                              'assets/manage.svg',
                              color: tabbarController.index.value == 0
                                  ? tabbarColor
                                  : Colors.black,
                              height: 25,
                              width: 25,
                            ),
                            Text(
                              "Quản lý chung",
                              style: TextStyle(
                                  fontSize: 10,
                                  color: tabbarController.index.value == 0
                                      ? tabbarColor
                                      : Colors.transparent),
                            )
                          ],
                        ),
                        label: 'Quản lý chung',
                      ),
                      BottomNavigationBarItem(
                        icon: Column(
                          children: [
                            SvgPicture.asset(
                              'assets/statistic.svg',
                              color: tabbarController.index.value == 1
                                  ? tabbarColor
                                  : Colors.black,
                              height: 25,
                              width: 25,
                            ),
                            Text(
                              "Thống kê",
                              style: TextStyle(
                                  fontSize: 10,
                                  color: tabbarController.index.value == 1
                                      ? tabbarColor
                                      : Colors.transparent),
                            )
                          ],
                        ),
                        label: 'Thống kê',
                      ),
                      BottomNavigationBarItem(
                        icon: Column(
                          children: [
                            SvgPicture.asset(
                              'assets/user.svg',
                              color: tabbarController.index.value == 2
                                  ? tabbarColor
                                  : Colors.black,
                              height: 25,
                              width: 25,
                            ),
                            Text(
                              "Người dùng",
                              style: TextStyle(
                                  fontSize: 10,
                                  color: tabbarController.index.value == 2
                                      ? tabbarColor
                                      : Colors.transparent),
                            )
                          ],
                        ),
                        label: 'Người dùng',
                      ),
                    ],
                    // iconSize: 35,
                    // selectedFontSize: 12,
                    currentIndex: tabbarController.index.value,
                    onTap: (index) {
                      tabbarController.change(index);
                      // print(index);
                    },
                  ),
                ),
              ));
        }
        return Scaffold(
            body: Obx(() => Container(
                  child: _widgetOptions.elementAt(tabbarController.index.value),
                )),
            bottomNavigationBar: Obx(
              () => SizedBox(
                height: 80,
                child: BottomNavigationBar(
                  showSelectedLabels: false,
                  showUnselectedLabels: false,
                  type: BottomNavigationBarType.fixed,
                  items: [
                    BottomNavigationBarItem(
                      icon: Column(
                        children: [
                          SvgPicture.asset(
                            'assets/home.svg',
                            color: tabbarController.index.value == 0
                                ? tabbarColor
                                : Colors.black,
                            height: 25,
                            width: 25,
                          ),
                          Text(
                            "Trang chủ",
                            style: TextStyle(
                                fontSize: 10,
                                color: tabbarController.index.value == 0
                                    ? tabbarColor
                                    : Colors.transparent),
                          )
                        ],
                      ),
                      label: 'Trang chủ',
                    ),
                    BottomNavigationBarItem(
                      icon: Column(
                        children: [
                          SvgPicture.asset(
                            'assets/shopping.svg',
                            color: tabbarController.index.value == 1
                                ? tabbarColor
                                : Colors.black,
                            height: 25,
                            width: 25,
                          ),
                          Text(
                            "Sản phẩm",
                            style: TextStyle(
                                fontSize: 10,
                                color: tabbarController.index.value == 1
                                    ? tabbarColor
                                    : Colors.transparent),
                          )
                        ],
                      ),
                      label: 'Sản phẩm',
                    ),
                    BottomNavigationBarItem(
                      icon: Column(
                        children: [
                          SvgPicture.asset(
                            'assets/news.svg',
                            color: tabbarController.index.value == 2
                                ? tabbarColor
                                : Colors.black,
                            height: 25,
                            width: 25,
                          ),
                          Text(
                            "Tin tức",
                            style: TextStyle(
                                fontSize: 10,
                                color: tabbarController.index.value == 2
                                    ? tabbarColor
                                    : Colors.transparent),
                          )
                        ],
                      ),
                      label: 'Tin tức',
                    ),
                    BottomNavigationBarItem(
                      // activeIcon: Icon(Icons.shopping_cart),
                      icon: BlocBuilder<CartBloc, CartState>(
                        builder: (context, state) {
                          return Stack(
                            children: [
                              Column(
                                children: [
                                  SvgPicture.asset(
                                    'assets/shoppingCart.svg',
                                    color: tabbarController.index.value == 3
                                        ? tabbarColor
                                        : Colors.black,
                                    height: 25,
                                    width: 25,
                                  ),
                                  Text(
                                    "Giỏ hàng",
                                    style: TextStyle(
                                        fontSize: 10,
                                        color: tabbarController.index.value == 3
                                            ? tabbarColor
                                            : Colors.transparent),
                                  )
                                ],
                              ),
                              tabbarController.index.value == 3
                                  ? SizedBox()
                                  : Positioned(
                                      right: 0,
                                      child: state.totalProduct != 0
                                          ? Container(
                                              width: 20,
                                              height: 20,
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color: tabbarColor),
                                              child: Center(
                                                child: Text(
                                                    '${state.totalProduct}',
                                                    style: TextStyle(
                                                        color: Colors.white)),
                                              ),
                                            )
                                          : Container())
                            ],
                          );
                        },
                      ),
                      label: 'Giỏ hàng',
                    ),
                    BottomNavigationBarItem(
                      icon: Column(
                        children: [
                          SvgPicture.asset(
                            'assets/user.svg',
                            color: tabbarController.index.value == 4
                                ? tabbarColor
                                : Colors.black,
                            height: 25,
                            width: 25,
                          ),
                          Text(
                            "Người dùng",
                            style: TextStyle(
                                fontSize: 10,
                                color: tabbarController.index.value == 4
                                    ? tabbarColor
                                    : Colors.transparent),
                          )
                        ],
                      ),
                      label: 'Người dùng',
                    ),
                  ],
                  // iconSize: 35,
                  // selectedFontSize: 12,
                  currentIndex: tabbarController.index.value,
                  onTap: (index) {
                    tabbarController.change(index);
                    // print(index);
                  },
                ),
              ),
            ));
      },
    );
  }
}
