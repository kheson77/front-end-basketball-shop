import 'package:basketballShop/Core/routes.dart';
import 'package:basketballShop/modules/login/bloc/login_bloc.dart';
import 'package:basketballShop/modules/userInfo/bloc/userinfo_bloc.dart';
import 'package:basketballShop/modules/userInfo/models/user.dart';
import 'package:basketballShop/widget/custom_button.dart';
import 'package:basketballShop/widget/custom_dialog.dart';
import 'package:basketballShop/widget/custom_textfiled.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toast/toast.dart';

class InfoUserScreen extends StatefulWidget {
  @override
  _InfoUserScreenState createState() => _InfoUserScreenState();
}

class _InfoUserScreenState extends State<InfoUserScreen> {
  List<String> _title_info = [
    "Họ đệm",
    "Tên",
    "Email",
    "Giới tính",
    "Số điện thoại",
    "Kiểu"
  ];

  TextEditingController _firstNameCtl = TextEditingController();
  TextEditingController _lastNameCtl = TextEditingController();
  TextEditingController _phoneNumberCtl = TextEditingController();
  String selectedWard;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  bool isEdit;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isEdit = false;
    selectedWard = BlocProvider.of<UserinfoBloc>(context).state.user.gender;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserinfoBloc, UserinfoState>(
      builder: (context, state) {
        List<String> _data_info = [
          state.user.firstName ?? "",
          state.user.lastName ?? "",
          state.user.email ?? "",
          state.user.gender ?? "",
          state.user.phoneNumber.toString() == "null"
              ? ""
              : state.user.phoneNumber.toString(),
          state.user.userType
        ];

        return Scaffold(
            key: _scaffoldKey,
            backgroundColor: Colors.white,
            appBar: AppBar(
              title: Text("Thông tin cá nhân",
                  style: TextStyle(color: Colors.black)),
              backgroundColor: Colors.white,
              actions: [
                IconButton(
                  icon: Icon(
                    Icons.edit,
                    color: isEdit == true ? Colors.blue : Colors.black,
                  ),
                  tooltip: 'Open shopping cart',
                  onPressed: () {
                    // handle the press
                    setState(() {
                      isEdit = !isEdit;
                    });
                  },
                  color: Colors.black,
                ),
              ],
              leading: IconButton(
                icon: Icon(Icons.arrow_back_outlined),
                color: Colors.black,
                onPressed: () {
                  // Get.back();
                  CoreRoutes.instance.pop();
                },
              ),
              elevation: 0,
            ),
            body: isEdit == false
                ? Container(
                    padding: EdgeInsets.all(15),
                    child: ListView.builder(
                      itemCount: _title_info.length,
                      itemBuilder: (context, index) {
                        return Container(
                          margin: EdgeInsets.only(top: 10),
                          padding: EdgeInsets.symmetric(
                              vertical: 15, horizontal: 15),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.blue),
                              borderRadius: BorderRadius.circular(15)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text(
                                  "${_title_info[index]}",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              Container(
                                child: Text("${_data_info[index]}"),
                              )
                            ],
                          ),
                        );
                      },
                    ),
                  )
                : Container(
                    padding: EdgeInsets.all(15),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: const EdgeInsets.fromLTRB(5, 10, 5, 5),
                            child: Text(
                              "Họ đệm",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 0),
                            child: CustomTextField.edit(
                              txtController: _firstNameCtl,
                              hintText: _data_info[0],
                              keyboardType: TextInputType.text,
                              onChanged: (String value) {
                                // check();
                              },
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.fromLTRB(5, 20, 5, 5),
                            child: Text(
                              "Tên",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 0),
                            child: CustomTextField.edit(
                              txtController: _lastNameCtl,
                              hintText: _data_info[1],
                              keyboardType: TextInputType.text,
                              onChanged: (String value) {
                                // check();
                              },
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.fromLTRB(5, 20, 5, 5),
                            child: Text(
                              "Email",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 0),
                            padding: EdgeInsets.only(left: 10),
                            height: 45,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                border: Border.all(color: Colors.grey)),
                            alignment: Alignment.centerLeft,
                            child: Text(
                              _data_info[2],
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.fromLTRB(5, 20, 5, 5),
                            child: Text(
                              "Giới tính",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 10, right: 10),
                            height: 40,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                border: Border.all(color: Colors.grey)),
                            child: Container(
                              // margin: EdgeInsets.only(left: 10, right: 10),
                              child: DropdownButton(
                                  // elevation: 0,
                                  isExpanded: true,
                                  value: selectedWard,
                                  icon: Icon(
                                    Icons.arrow_drop_down,
                                    size: 25,
                                  ),
                                  underline: SizedBox(),
                                  items: ["Nam", "Nữ"]
                                      .map<DropdownMenuItem<String>>(
                                          (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text("${value}"),
                                    );
                                  }).toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      selectedWard = value;
                                    });
                                    // print(selectedWard);
                                  }),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.fromLTRB(5, 20, 5, 5),
                            child: Text(
                              "Số điện thoại",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 0),
                            child: CustomTextField.edit(
                              txtController: _phoneNumberCtl,
                              hintText: _data_info[4],
                              keyboardType: TextInputType.text,
                              onChanged: (String value) {
                                // check();
                              },
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.fromLTRB(5, 20, 5, 5),
                            child: Text(
                              "Kiểu",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 0),
                            padding: EdgeInsets.only(left: 10),
                            height: 45,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                border: Border.all(color: Colors.grey)),
                            alignment: Alignment.centerLeft,
                            child: Text(
                              _data_info[5],
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(vertical: 20),
                            child: CustomButton('Chỉnh sửa', () {
                              if (_firstNameCtl.text == '' &&
                                  _lastNameCtl.text == '' &&
                                  _phoneNumberCtl.text == '' &&
                                  selectedWard == state.user.gender) {
                                _scaffoldKey.currentState.showSnackBar(SnackBar(
                                    content: Text(
                                        "Bạn phải thay đổi mới có thể cập nhật")));
                                return;
                              }
                              User user = User(
                                  firstName: _firstNameCtl.text == ''
                                      ? state.user.firstName
                                      : _firstNameCtl.text,
                                  lastName: _lastNameCtl.text == ''
                                      ? state.user.lastName
                                      : _lastNameCtl.text,
                                  gender: selectedWard,
                                  phoneNumber: _phoneNumberCtl.text == ''
                                      ? state.user.phoneNumber
                                      : int.parse(_phoneNumberCtl.text));

                              showDialog(
                                  context: context,
                                  builder: (_) {
                                    // print(user.firstName);
                                    return CustomDialog.user(
                                      image: state.user.userThumbnail,
                                      name: state.user.lastName,
                                      actionText: "cập nhật",
                                      onConfirm: () {
                                        Navigator.of(context).pop();
                                        Toast.show(
                                            "Cập nhật thành công!", context,
                                            duration: Toast.LENGTH_SHORT,
                                            gravity: Toast.BOTTOM);
                                        BlocProvider.of<UserinfoBloc>(context)
                                            .add(UserInfoUpdateInfoEvent(user));
                                        Navigator.of(context).pop();
                                      },
                                    );
                                  });
                            }, Colors.blue),
                          ),
                        ],
                      ),
                    )));
      },
    );
  }
}
