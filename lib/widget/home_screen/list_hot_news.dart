import 'package:basketballShop/Core/routes.dart';
import 'package:basketballShop/modules/news/bloc/news_bloc.dart';
import 'package:basketballShop/modules/tabbar/tabbar_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';

class ListHotNews extends StatelessWidget {
  TabbarController tabbarController = Get.find();
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: ListTile(
                title: Text('Tin tức mới nhất'),
              ),
            ),
            Container(
                margin: EdgeInsets.only(right: 20),
                child: GestureDetector(
                    onTap: () {
                      //TODO: Chuyển Tab
                      tabbarController.index.value = 2;
                    },
                    child: Text(
                      'Xem thêm',
                      style: TextStyle(color: Colors.blue),
                    ))),
          ],
        ),
        Container(
          height: 150,
          // margin: EdgeInsets.only(top: 10),
          child: BlocBuilder<NewsBloc, NewsState>(
            builder: (context, state) {
              if (state is NewsInitial) {
                return ListHotNewsLoading();
              }
              return ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: state.news.length,
                itemBuilder: (context, i) {
                  return Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    margin: EdgeInsets.only(left: 20),
                    child: InkWell(
                      onTap: () {
                        CoreRoutes.instance.navigateTo(
                            CoreRouteNames.NEWS_DETAIL,
                            arguments: state.news[i]);
                      },
                      borderRadius: BorderRadius.circular(8),
                      splashColor: Colors.orange,
                      child: Container(
                        width: width * 0.85,
                        // height: 200,
                        // padding: EdgeInsets.only(right: 20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            image: DecorationImage(
                                image:
                                    NetworkImage("${state.news[i].thumbnail}"),
                                fit: BoxFit.cover)),
                        alignment: Alignment.bottomLeft,
                        child: Container(
                          padding:
                              EdgeInsets.only(top: 10, left: 10, bottom: 10),
                          height: 60,
                          decoration: BoxDecoration(
                              color: Colors.black.withOpacity(0.5),
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(20),
                                  bottomRight: Radius.circular(20))),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "${state.news[i].title}",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                              Text(
                                "${state.news[i].shortContent}",
                                style: TextStyle(color: Colors.white),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
              );
            },
          ),
        ),
      ],
    );
  }
}

class ListHotNewsLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Shimmer.fromColors(
      baseColor: Colors.grey[300],
      highlightColor: Colors.grey[100],
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 3,
        itemBuilder: (context, index) {
          return Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              margin: EdgeInsets.only(left: 20),
              child: Container(
                width: width * 0.85,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white),
              ));
        },
      ),
    );
  }
}
