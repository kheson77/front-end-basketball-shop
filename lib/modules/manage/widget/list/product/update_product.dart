import 'dart:io';

import 'package:basketballShop/modules/manage/bloc/manage_bloc.dart';
import 'package:basketballShop/modules/product/models/product.dart';
import 'package:basketballShop/widget/custom_button.dart';
import 'package:basketballShop/widget/custom_dialog.dart';
import 'package:basketballShop/widget/custom_textfiled.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:toast/toast.dart';
import 'package:intl/intl.dart';

class UpdateProduct extends StatefulWidget {
  final Product product;
  UpdateProduct({this.product});

  @override
  _UpdateProductState createState() => _UpdateProductState();
}

class _UpdateProductState extends State<UpdateProduct> {
  bool isEdit;
  TextEditingController _nameProduct = TextEditingController();
  TextEditingController _priceProduct = TextEditingController();
  TextEditingController _countProduct = TextEditingController();
  TextEditingController _descriptionProduct = TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int selectCate;
  String selectStatus;

  File _image;

  final picker = ImagePicker();

  Future getImageFromCamera() async {
    final pickedImage =
        await picker.getImage(source: ImageSource.camera, imageQuality: 100);

    setState(() {
      if (pickedImage != null) {
        _image = File(pickedImage.path);
      } else {
        print("No Image Selected");
      }
    });
  }

  Future getImageFromGallery() async {
    final pickedImage =
        await picker.getImage(source: ImageSource.gallery, imageQuality: 100);

    setState(() {
      if (pickedImage != null) {
        _image = File(pickedImage.path);
      } else {
        print("No Image Selected");
      }
    });
  }

  @override
  void initState() {
    super.initState();
    isEdit = false;
    selectCate = widget.product.idCategory;
    selectStatus = widget.product.status;
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            "${widget.product.name}",
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
          actions: [
            IconButton(
              icon: Icon(
                Icons.edit,
                color: !isEdit ? Colors.black : Colors.blue,
              ),
              onPressed: () {
                this.setState(() {
                  isEdit = !isEdit;
                });
              },
              color: Colors.black,
            ),
          ],
          leading: IconButton(
            icon: Icon(Icons.arrow_back_outlined),
            color: Colors.black,
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          elevation: 0,
        ),
        body: Stack(children: [
          SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                Stack(
                  children: [
                    Container(
                      width: width,
                      margin: EdgeInsets.symmetric(vertical: 10),
                      alignment: Alignment.center,
                      child: Container(
                        width: 150,
                        height: 150,
                        alignment: Alignment.bottomRight,
                        child: !isEdit
                            ? SizedBox()
                            : Container(
                                padding: EdgeInsets.only(right: 20),
                                child: Container(
                                    height: 30,
                                    width: 30,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.white),
                                    child: GestureDetector(
                                        onTap: () {
                                          showModalBottomSheet(
                                            context: context,
                                            builder: ((builder) =>
                                                _bottomSheet()),
                                          );
                                        },
                                        child: Icon(Icons.camera_alt)))),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: _image != null
                                    ? FileImage(_image)
                                    : widget.product.thumbnail == null ||
                                            widget.product.thumbnail == ""
                                        ? AssetImage('assets/default_image.jpg')
                                        : NetworkImage(
                                            widget.product.thumbnail),
                                fit: BoxFit.cover)),
                      ),
                    ),
                    Positioned(
                        right: 20,
                        bottom: 10,
                        child: Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: Colors.red),
                          child: IconButton(
                              color: Colors.white,
                              onPressed: () {
                                showDialog(
                                    context: context,
                                    builder: (_) {
                                      return CustomDialog.product(
                                        image: widget.product.thumbnail,
                                        name: widget.product.name,
                                        actionText: "Xóa",
                                        onConfirm: () {
                                          // print(widget.keyListScreen);
                                          Navigator.of(context).pop();
                                          Navigator.of(context).pop();
                                          Toast.show("Xóa thành công!", context,
                                              duration: Toast.LENGTH_SHORT,
                                              gravity: Toast.BOTTOM);
                                          BlocProvider.of<ManageBloc>(context)
                                              .add(DeleteProductEvent(
                                                  widget.product.idProduct));
                                        },
                                      );
                                    });
                              },
                              icon: Icon(Icons.delete)),
                        ))
                  ],
                ),
                isEdit ? editProduct() : viewProduct()
              ])),
          !isEdit
              ? SizedBox()
              : Positioned(
                  bottom: 0,
                  child: Container(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    color: Colors.grey.withOpacity(0.8),
                    child: CustomButton('Chỉnh sửa', () {
                      if (_nameProduct.text == '' &&
                          selectStatus == widget.product.status &&
                          _countProduct.text == '' &&
                          _priceProduct.text == '' &&
                          _descriptionProduct.text == '' &&
                          selectCate == widget.product.idCategory &&
                          _image == null) {
                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                            content:
                                Text("Bạn phải thay đổi mới có thể cập nhật")));
                        return;
                      }
                      showDialog(
                          context: context,
                          builder: (_) {
                            return CustomDialog.product(
                              image: widget.product.thumbnail,
                              name: widget.product.name,
                              actionText: "cập nhật",
                              onConfirm: () {
                                Navigator.of(context).pop();
                                BlocProvider.of<ManageBloc>(context).add(
                                    UpdateProductEvent(dataProduct(),
                                        _image != null ? true : false));
                                Navigator.of(context).pop();
                                Toast.show("Cập nhật thành công!", context,
                                    duration: Toast.LENGTH_SHORT,
                                    gravity: Toast.BOTTOM);
                              },
                            );
                          });
                    }, Colors.blue),
                  ),
                ),
        ]));
  }

  List<String> _title_info_product = [
    "Tên sản phẩm",
    "Giờ chỉnh sửa",
    "Ngày chỉnh sửa",
    "Trạng thái",
    "Loại sản phẩm",
    "Số lượng",
    "Giá",
    "Mô tả",
  ];

  Widget viewProduct() {
    var height = MediaQuery.of(context).size.height;
    List<String> _data_info_product = [
      widget.product.name,
      DateFormat('kk:mm').format(widget.product.modifiedDate),
      DateFormat('dd-MM-yyyy').format(widget.product.modifiedDate),
      getStatus(widget.product.status),
      getCategory(widget.product.idCategory),
      widget.product.count.toString(),
      widget.product.price.toString(),
      widget.product.description
    ];
    return Container(
      height: height - 100,
      padding: EdgeInsets.all(15),
      child: ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
        itemCount: _title_info_product.length,
        itemBuilder: (context, index) {
          return index == 7
              ? Container(
                  margin: EdgeInsets.only(top: 10),
                  padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.blue),
                      borderRadius: BorderRadius.circular(15)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 15),
                        child: Text(
                          "${_title_info_product[index]}",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        child: Text(
                          "${_data_info_product[index]}",
                          maxLines: 10,
                        ),
                      )
                    ],
                  ),
                )
              : Container(
                  margin: EdgeInsets.only(top: 10),
                  padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.blue),
                      borderRadius: BorderRadius.circular(15)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text(
                          "${_title_info_product[index]}",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        child: Text(
                          "${_data_info_product[index]}",
                          style: TextStyle(
                            color: index != 3
                                ? Colors.black
                                : widget.product.status == 'active'
                                    ? Colors.blue
                                    : Colors.red,
                          ),
                        ),
                      )
                    ],
                  ),
                );
        },
      ),
    );
  }

  Widget editProduct() {
    var width = MediaQuery.of(context).size.width;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
          child: Text("Tên sản phẩm"),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
          ),
          child: CustomTextField.edit(
            txtController: _nameProduct,
            hintText: '${widget.product.name}',
            keyboardType: TextInputType.text,
            onChanged: (String value) {
              // check();
            },
          ),
        ),
        Container(
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
          child: Text("Trạng thái"),
        ),
        Container(
          margin: EdgeInsets.only(left: 20),
          child: Container(
            width: width - 40,
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              border: Border.all(
                  color: Colors.black, style: BorderStyle.solid, width: 0.7),
            ),
            child: Container(
              margin: EdgeInsets.only(left: 10, right: 10),
              child: DropdownButton(
                  // elevation: 0,
                  isExpanded: true,
                  value: selectStatus,
                  icon: Icon(
                    Icons.arrow_drop_down,
                    size: 25,
                  ),
                  underline: SizedBox(),
                  items: ['active', 'inactive']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(getStatus(value)),
                    );
                  }).toList(),
                  onChanged: (value) {
                    setState(() {
                      selectStatus = value;
                    });
                  }),
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
          child: Text("Loại sản phẩm"),
        ),
        Container(
          margin: EdgeInsets.only(left: 20),
          child: Container(
            width: width - 40,
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              border: Border.all(
                  color: Colors.black, style: BorderStyle.solid, width: 0.7),
            ),
            child: Container(
              margin: EdgeInsets.only(left: 10, right: 10),
              child: DropdownButton(
                  // elevation: 0,
                  isExpanded: true,
                  value: selectCate,
                  icon: Icon(
                    Icons.arrow_drop_down,
                    size: 25,
                  ),
                  underline: SizedBox(),
                  items: [5, 6, 7, 8].map<DropdownMenuItem<int>>((int value) {
                    return DropdownMenuItem<int>(
                      value: value,
                      child: Text(getCategory(value)),
                    );
                  }).toList(),
                  onChanged: (value) {
                    setState(() {
                      selectCate = value;
                    });
                    // print(selectCate);
                  }),
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
          child: Text("Số lượng"),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
          ),
          child: CustomTextField.edit(
            txtController: _countProduct,
            hintText: '${widget.product.count}',
            keyboardType: TextInputType.text,
            onChanged: (String value) {
              // check();
            },
          ),
        ),
        Container(
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
          child: Text("Giá sản phẩm"),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
          ),
          child: CustomTextField.edit(
            txtController: _priceProduct,
            hintText: '${widget.product.price}',
            keyboardType: TextInputType.text,
            onChanged: (String value) {
              // check();
            },
          ),
        ),
        Container(
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
          child: Text("Mô tả"),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          height: 160,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
          ),
          child: CustomTextField.edit(
            txtController: _descriptionProduct,
            hintText: widget.product.description,
            keyboardType: TextInputType.text,
            onChanged: (String value) {
              // check();
            },
          ),
        ),
        SizedBox(height: 120),
      ],
    );
  }

  String getCategory(int i) {
    if (i == 5) {
      return "Giày bóng rổ";
    } else if (i == 6) {
      return "Quần áo bóng rổ";
    } else if (i == 7) {
      return "Phụ kiện bóng rổ";
    } else if (i == 8) {
      return "Quả bóng rổ";
    }
  }

  String getStatus(String s) {
    if (s == 'active') {
      return 'Hoạt động';
    } else if (s == 'inactive') {
      return 'Không hoạt động';
    }
  }

  Product dataProduct() {
    Product product = Product(
      idProduct: widget.product.idProduct,
      name: _nameProduct.text == '' ? widget.product.name : _nameProduct.text,
      status: selectStatus,
      count: _countProduct.text == ''
          ? widget.product.count
          : int.parse(_countProduct.text),
      price: _priceProduct.text == ''
          ? widget.product.price
          : double.parse(_priceProduct.text),
      description: _descriptionProduct.text == ''
          ? widget.product.description
          : _descriptionProduct.text,
      thumbnail: _image != null ? _image.path : widget.product.thumbnail,
      idCategory: selectCate,
    );
    return product;
  }

  Widget _bottomSheet() {
    return Container(
      height: 100,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Column(
        children: [
          Text(
            "Choose Profile Photo",
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              FlatButton.icon(
                onPressed: getImageFromCamera,
                icon: Icon(Icons.camera),
                label: Text("Camera"),
              ),
              FlatButton.icon(
                onPressed: getImageFromGallery,
                icon: Icon(Icons.image),
                label: Text("Galery"),
              )
            ],
          )
        ],
      ),
    );
  }
}
