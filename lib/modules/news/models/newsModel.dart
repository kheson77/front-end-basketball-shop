// To parse this JSON data, do
//
//     final newsModel = newsModelFromJson(jsonString);

import 'dart:convert';

NewsModel newsModelFromJson(String str) => NewsModel.fromJson(json.decode(str));

String newsModelToJson(NewsModel data) => json.encode(data.toJson());

class NewsModel {
  NewsModel({
    this.count,
    this.next,
    this.previous,
    this.results,
  });

  int count;
  dynamic next;
  dynamic previous;
  List<News> results;

  factory NewsModel.fromJson(Map<String, dynamic> json) => NewsModel(
        count: json["count"],
        next: json["next"],
        previous: json["previous"],
        results: List<News>.from(json["results"].map((x) => News.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "next": next,
        "previous": previous,
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
      };
}

class News {
  News({
    this.idNews,
    this.title,
    this.thumbnail,
    this.shortContent,
    this.content,
    this.dateModified,
  });

  int idNews;
  String title;
  String thumbnail;
  String shortContent;
  String content;
  DateTime dateModified;

  factory News.fromJson(Map<String, dynamic> json) => News(
        idNews: json["idNews"],
        title: json["title"],
        thumbnail: json["thumbnail"],
        shortContent: json["shortContent"],
        content: json["content"],
        dateModified: DateTime.parse(json["dateModified"]),
      );

  Map<String, dynamic> toJson() => {
        "idNews": idNews,
        "title": title,
        "thumbnail": thumbnail,
        "shortContent": shortContent,
        "content": content,
        "dateModified": dateModified.toIso8601String(),
      };
}
