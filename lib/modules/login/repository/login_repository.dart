import 'dart:convert';

import 'package:basketballShop/const/baseUrl.dart';
import 'package:basketballShop/modules/login/model/user.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';

class LoginRepository {
  Future<User> login(String username, String password) async {
    Map body = {"email": username, "password": password};
    final encoding = Encoding.getByName('utf-8');
    final res = await http.post(Uri.parse(API_LOGIN),
        headers: {'Content-Type': 'application/json'},
        body: json.encode(body),
        encoding: encoding);
    print("status code: ${res.statusCode}");
    if (res.statusCode == 200) {
      print("Đăng nhập thành công");
      return userFromJson(res.body);
      // return 200;
    } else {
      print("Đăng nhập thất bại");
      return null;
    }
  }

  Future<void> register(String name, String username, String password) async {
    Map body = {"lastName": name, "email": username, "password": password};
    final encoding = Encoding.getByName('utf-8');
    var res = await http.post(Uri.parse(API_REGISTER),
        headers: {'Content-Type': 'application/json'},
        body: json.encode(body),
        encoding: encoding);
    print("status code: ${res.statusCode}");
    if (res.statusCode == 200) {
      print("Đăng ký thành công");
    } else {
      print("Đăng ký thất bại");
    }
  }

  Future<User> getUserById(int id, accessToken) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = accessToken;
    Response response = await dio.get("${API_GET_USER}${id}");
    if (response.statusCode == 200) {
      return User.fromJson(response.data);
    } else {
      return null;
    }
  }

  Future<String> updateImage(int id, String image) async {
    var userThumbnail = await http.MultipartFile.fromPath(
        "userThumbnail", image,
        contentType: MediaType("image", "png"));
    var request = http.MultipartRequest(
        "PATCH", Uri.parse('${API_UPDATE_IMAGE_USER}${id}'));
    request.files.add(userThumbnail);
    request.send().then((response) {
      print(response.statusCode);
      print("thêm thành công");
    }).catchError((error) => print(error));
  }
}
