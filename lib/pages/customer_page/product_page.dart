import 'package:basketballShop/modules/product/bloc/product_bloc.dart';
import 'package:basketballShop/modules/product/models/product.dart';
import 'package:basketballShop/modules/product/widgets/product_list.dart';
import 'package:basketballShop/modules/shopping_cart/bloc/cart_bloc.dart';
import 'package:basketballShop/modules/tabbar/category_tabbar_controller.dart';
import 'package:basketballShop/modules/tabbar/tabbar_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

class ProductPage extends StatefulWidget {
  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage>
    with TickerProviderStateMixin {
  TabController _tabController;

  CategoryTabbarController categoryTabbarController = Get.find();

  @override
  void initState() {
    super.initState();
    // categoryTabbarController = Get.put(CategoryTabbarController());
    _tabController = new TabController(
        vsync: this,
        length: 4,
        initialIndex: categoryTabbarController.index.value ?? 0);
    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {});
    categoryTabbarController.index.value = _tabController.index;
  }

  List<String> title = [
    "Giày bóng rổ",
    "Đồng phục bóng rổ",
    "Phụ kiện bóng rổ",
    "Quả bóng rổ"
  ];

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return DefaultTabController(
      length: 4,
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: new Size.fromHeight(height / 3),
            child: AppBar(
              backgroundColor: Colors.transparent,
              // bottomOpacity: 0.0,
              elevation: 0.0,
              centerTitle: true,
              leading: Icon(
                Icons.person_outline,
                color: Colors.black,
              ),
              actions: [
                Padding(
                  padding: const EdgeInsets.only(right: 16.0),
                  child: Stack(
                    children: [
                      Center(
                        child: ClipOval(
                          child: Material(
                            color: Colors.red.withOpacity(0.7), // button color
                            child: InkWell(
                              splashColor: Colors.yellow, // inkwell color
                              child: SizedBox(
                                  width: 40,
                                  height: 40,
                                  child: Icon(
                                    Icons.notifications,
                                    color: Colors.white,
                                    size: 35,
                                  )),
                              onTap: () {},
                            ),
                          ),
                        ),
                      ),
                      BlocBuilder<CartBloc, CartState>(
                        builder: (context, state) {
                          return Container(
                            width: 40,
                            child: Center(
                                child: Text(
                              state.totalProduct != 0
                                  ? state.totalProduct.toString()
                                  : "",
                              style: TextStyle(color: Colors.red),
                            )),
                          );
                        },
                      )
                    ],
                  ),
                ),
              ],
              title: Text(
                'Danh mục',
                style: TextStyle(fontSize: 16.0, color: Colors.black),
              ),
              bottom: PreferredSize(
                preferredSize: Size.fromHeight(height / 3),
                child: Column(
                  children: [
                    TabBar(
                        isScrollable: true,
                        unselectedLabelColor: Colors.black.withOpacity(0.3),
                        labelColor: Colors.black,
                        controller: _tabController,
                        // indicatorColor: Colors.transparent,
                        indicator: BoxDecoration(),
                        labelPadding: EdgeInsets.only(left: 5),
                        tabs: [
                          Container(
                            height: 150,
                            width: 160,
                            child: Tab(
                                child: Column(
                              children: [
                                Container(
                                  height: 120,
                                  width: 150,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                        colorFilter: _tabController.index == 0
                                            ? ColorFilter.mode(
                                                Colors.black.withOpacity(1),
                                                BlendMode.dstATop)
                                            : ColorFilter.mode(
                                                Colors.black.withOpacity(0.5),
                                                BlendMode.dstATop),
                                        image:
                                            ExactAssetImage('assets/shoe.jpg'),
                                        fit: BoxFit.cover,
                                      ),
                                      boxShadow: [
                                        BoxShadow(
                                          color: _tabController.index == 0
                                              ? Colors.grey.withOpacity(0.5)
                                              : Colors.transparent,
                                          spreadRadius: 3,
                                          blurRadius: 5,
                                          offset: Offset(2,
                                              3), // changes position of shadow
                                        ),
                                      ],
                                      borderRadius: BorderRadius.circular(10)),
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 5),
                                  child: Text("Giày bóng rổ"),
                                )
                              ],
                            )),
                          ),
                          Container(
                            height: 150,
                            width: 160,
                            child: Tab(
                                child: Column(
                              children: [
                                Container(
                                  height: 120,
                                  width: 150,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                        colorFilter: _tabController.index == 1
                                            ? ColorFilter.mode(
                                                Colors.black.withOpacity(1),
                                                BlendMode.dstATop)
                                            : ColorFilter.mode(
                                                Colors.black.withOpacity(0.5),
                                                BlendMode.dstATop),
                                        image: ExactAssetImage(
                                            'assets/jersey.jpg'),
                                        fit: BoxFit.cover,
                                      ),
                                      boxShadow: [
                                        BoxShadow(
                                          color: _tabController.index == 1
                                              ? Colors.grey.withOpacity(0.5)
                                              : Colors.transparent,
                                          spreadRadius: 3,
                                          blurRadius: 5,
                                          offset: Offset(2,
                                              3), // changes position of shadow
                                        ),
                                      ],
                                      borderRadius: BorderRadius.circular(10)),
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 5),
                                  child: Text("Đồng phục bóng rổ"),
                                )
                              ],
                            )),
                          ),
                          Container(
                            height: 150,
                            width: 160,
                            child: Tab(
                                child: Column(
                              children: [
                                Container(
                                  height: 120,
                                  width: 150,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                        colorFilter: _tabController.index == 2
                                            ? ColorFilter.mode(
                                                Colors.black.withOpacity(1),
                                                BlendMode.dstATop)
                                            : ColorFilter.mode(
                                                Colors.black.withOpacity(0.5),
                                                BlendMode.dstATop),
                                        image: ExactAssetImage(
                                            'assets/accessories.jpg'),
                                        fit: BoxFit.cover,
                                      ),
                                      boxShadow: [
                                        BoxShadow(
                                          color: _tabController.index == 2
                                              ? Colors.grey.withOpacity(0.5)
                                              : Colors.transparent,
                                          spreadRadius: 3,
                                          blurRadius: 5,
                                          offset: Offset(2,
                                              3), // changes position of shadow
                                        ),
                                      ],
                                      borderRadius: BorderRadius.circular(10)),
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 5),
                                  child: Text("Phụ kiện bóng rổ"),
                                )
                              ],
                            )),
                          ),
                          Container(
                            height: 150,
                            width: 160,
                            child: Tab(
                                child: Column(
                              children: [
                                Container(
                                  height: 120,
                                  width: 150,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                        colorFilter: _tabController.index == 3
                                            ? ColorFilter.mode(
                                                Colors.black.withOpacity(1),
                                                BlendMode.dstATop)
                                            : ColorFilter.mode(
                                                Colors.black.withOpacity(0.5),
                                                BlendMode.dstATop),
                                        image: ExactAssetImage(
                                            'assets/basketball.jpg'),
                                        fit: BoxFit.cover,
                                      ),
                                      boxShadow: [
                                        BoxShadow(
                                          color: _tabController.index == 3
                                              ? Colors.grey.withOpacity(0.5)
                                              : Colors.transparent,
                                          spreadRadius: 3,
                                          blurRadius: 5,
                                          offset: Offset(2,
                                              3), // changes position of shadow
                                        ),
                                      ],
                                      borderRadius: BorderRadius.circular(10)),
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 5),
                                  child: Text("Quả bóng rổ"),
                                )
                              ],
                            )),
                          ),
                        ]),
                    Container(
                      padding: EdgeInsets.only(left: 10, right: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            padding: EdgeInsets.only(bottom: 10, left: 5),
                            child: Text(
                              "${title[_tabController.index]}",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(bottom: 10, right: 5),
                            child: Text(
                              "Bộ lọc",
                              style: TextStyle(fontSize: 14),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          body: BlocBuilder<ProductBloc, ProductState>(
            builder: (context, state) {
              return TabBarView(
                controller: _tabController,
                children: <Widget>[
                  ProductList(
                    title: "Giày bóng rổ",
                    products: state.shoes,
                  ),
                  ProductList(
                    title: "Giày bóng rổ",
                    products: state.jerseys,
                  ),
                  ProductList(
                    title: "Giày bóng rổ",
                    products: state.accessories,
                  ),
                  ProductList(
                    title: "Giày bóng rổ",
                    products: state.basketball,
                  ),
                ],
              );
            },
          )),
    );
  }
}
