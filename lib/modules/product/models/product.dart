// To parse this JSON data, do
//
//     final productModel = productModelFromJson(jsonString);

import 'dart:convert';

ProductModel productModelFromJson(String str) =>
    ProductModel.fromJson(json.decode(str));

String productModelToJson(ProductModel data) => json.encode(data.toJson());

class ProductModel {
  ProductModel({
    this.count,
    this.next,
    this.previous,
    this.results,
  });

  int count;
  dynamic next;
  dynamic previous;
  List<Product> results;

  factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
        count: json["count"],
        next: json["next"],
        previous: json["previous"],
        results:
            List<Product>.from(json["results"].map((x) => Product.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "next": next,
        "previous": previous,
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
      };
}

class Product {
  Product({
    this.idProduct,
    this.name,
    this.price,
    this.thumbnail,
    this.count,
    this.status,
    this.description,
    this.modifiedDate,
    this.idCategory,
  });

  int idProduct;
  String name;
  double price;
  String thumbnail;
  String description;
  int count;
  String status;
  DateTime modifiedDate;
  int idCategory;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        idProduct: json["idProduct"],
        name: json["name"],
        price: json["price"].toDouble(),
        thumbnail: json["thumbnail"],
        description: json["description"],
        count: json['count'],
        status: json['status'],
        modifiedDate: DateTime.parse(json["modifiedDate"]),
        idCategory: json["idCategory"],
      );

  Map<String, dynamic> toJson() => {
        "idProduct": idProduct,
        "name": name,
        "price": price,
        "thumbnail": thumbnail,
        "description": description,
        'count': count,
        'status': status,
        "modifiedDate": modifiedDate.toIso8601String(),
        "idCategory": idCategory,
      };
}
