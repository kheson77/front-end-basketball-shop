import 'package:basketballShop/Core/routes.dart';
import 'package:basketballShop/modules/manage/bloc/manage_bloc.dart';
import 'package:basketballShop/widget/loading_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ManageDashboard extends StatelessWidget {
  List<String> _title = ["USER", "PRODUCT", "ORDER", "SHOPPING CART", "NEWS"];
  List<String> _subTitle = [
    "Người dùng",
    "Sản phẩm",
    "Đơn hàng",
    "Giỏ hàng",
    "Bài viết"
  ];

  List<String> _image = [
    'assets/girl.jpg',
    'assets/shoe.jpg',
    'assets/shoe.jpg',
    'assets/shoe.jpg',
    'assets/news.jpg'
  ];
  List<int> _countManage;

  var _argument;

  List<String> _routeOption = [
    CoreRouteNames.MANAGE_ACCOUNT_TAB,
    CoreRouteNames.MANAGE_PRODUCT_TAB,
    '',
    '',
    CoreRouteNames.MANAGE_NEWS_LIST,
  ];

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return BlocBuilder<ManageBloc, ManageState>(
      builder: (context, state) {
        if (state is ManageInitial) {
          _countManage = [0, 0, 0, 0, 0];
          return LoadingList();
        } else {
          _countManage = [
            state.accounts.length,
            state.products.length,
            0,
            state.carts.length,
            state.news.length
          ];
        }

        return Expanded(
          child: ListView.builder(
            padding: EdgeInsets.zero,
            itemCount: _title.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  CoreRoutes().navigateTo(_routeOption[index]);
                },
                child: Container(
                  margin: EdgeInsets.fromLTRB(0, 20, 40, 0),
                  padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                  height: 100,
                  width: width,
                  alignment: Alignment.centerRight,
                  decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30),
                          bottomRight: Radius.circular(30))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        height: 90,
                        width: 90,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                            image: DecorationImage(
                                image: AssetImage(_image[index]),
                                fit: BoxFit.cover)),
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "${_title[index]}",
                              style: TextStyle(color: Colors.white),
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  _countManage[index].toString(),
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 38),
                                ),
                                Container(
                                  padding: EdgeInsets.all(8),
                                  child: Text(
                                    "${_subTitle[index]}",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Spacer(),
                      Container(
                        margin: EdgeInsets.all(15),
                        alignment: Alignment.topCenter,
                        height: 100,
                        child: Icon(Icons.star, color: Colors.amberAccent),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        );
      },
    );
  }
}
