import 'package:flutter/material.dart';

class SelectColor extends StatefulWidget {
  @override
  _SelectColorState createState() => _SelectColorState();
}

class _SelectColorState extends State<SelectColor> {
  int _indexColor;
  @override
  void initState() {
    super.initState();
    _indexColor = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 5, left: 24, right: 24),
      child: Row(
        children: [
          InkWell(
            onTap: () {
              setState(() {
                _indexColor = 0;
              });
            },
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                  border: Border.all(width: 2, color: Colors.red[900]),
                  color: Colors.red,
                  shape: BoxShape.circle),
              alignment: Alignment.center,
              child: Container(
                width: 35,
                height: 35,
                child: _indexColor == 0
                    ? Icon(Icons.done, color: Colors.white)
                    : SizedBox(),
              ),
            ),
          ),
          SizedBox(width: 15),
          InkWell(
            onTap: () {
              setState(() {
                _indexColor = 1;
              });
            },
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                  border: Border.all(width: 2, color: Colors.black54),
                  color: Colors.white,
                  shape: BoxShape.circle),
              alignment: Alignment.center,
              child: Container(
                width: 35,
                height: 35,
                child: _indexColor == 1
                    ? Icon(Icons.done, color: Colors.black)
                    : SizedBox(),
              ),
            ),
          ),
          SizedBox(width: 15),
          InkWell(
            onTap: () {
              setState(() {
                _indexColor = 2;
              });
            },
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                  border: Border.all(width: 2, color: Colors.black),
                  color: Colors.black87,
                  shape: BoxShape.circle),
              alignment: Alignment.center,
              child: Container(
                width: 35,
                height: 35,
                child: _indexColor == 2
                    ? Icon(Icons.done, color: Colors.white)
                    : SizedBox(),
              ),
            ),
          )
        ],
      ),
    );
  }
}
