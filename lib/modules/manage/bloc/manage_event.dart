part of 'manage_bloc.dart';

@immutable
abstract class ManageEvent {}

class FirstLoadManageEvent extends ManageEvent {}

class GetAllManageEvent extends ManageEvent {}

class UpdateProductEvent extends ManageEvent {
  final Product product;
  final bool upLoadImage;
  UpdateProductEvent(this.product, this.upLoadImage);
}

class AddProductEvent extends ManageEvent {
  final Product product;
  AddProductEvent(this.product);
}

class DeleteProductEvent extends ManageEvent {
  final int idProduct;
  DeleteProductEvent(this.idProduct);
}

class UpdateAccountEvent extends ManageEvent {
  final User account;
  UpdateAccountEvent(this.account);
}

class DeleteAccountEvent extends ManageEvent {
  final int idAccount;
  DeleteAccountEvent(this.idAccount);
}
