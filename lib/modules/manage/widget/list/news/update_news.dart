import 'dart:io';

import 'package:basketballShop/modules/news/models/newsModel.dart';
import 'package:basketballShop/widget/custom_button.dart';
import 'package:basketballShop/widget/custom_dialog.dart';
import 'package:basketballShop/widget/custom_textfiled.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:toast/toast.dart';

class UpdateNews extends StatefulWidget {
  final News news;
  UpdateNews({this.news});

  @override
  _UpdateNewsState createState() => _UpdateNewsState();
}

class _UpdateNewsState extends State<UpdateNews> {
  TextEditingController _titleNews = TextEditingController();
  TextEditingController _contentNews = TextEditingController();
  TextEditingController _shortContentNews = TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  File _image;
  final picker = ImagePicker();

  Future getImageFromCamera() async {
    final pickedImage =
        await picker.getImage(source: ImageSource.camera, imageQuality: 100);

    setState(() {
      if (pickedImage != null) {
        _image = File(pickedImage.path);
      } else {
        print("No Image Selected");
      }
    });
  }

  Future getImageFromGallery() async {
    final pickedImage =
        await picker.getImage(source: ImageSource.gallery, imageQuality: 100);

    setState(() {
      if (pickedImage != null) {
        _image = File(pickedImage.path);
      } else {
        print("No Image Selected");
      }
    });
  }

  final _validate =
      MultiValidator([RequiredValidator(errorText: 'Không được để trống')]);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var image = widget.news.thumbnail;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          "${widget.news.title}",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        actions: [
          IconButton(
            icon: Icon(Icons.delete),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (_) {
                    return CustomDialog.news(
                      image: widget.news.thumbnail,
                      name: widget.news.title,
                      actionText: "Xóa",
                      onConfirm: () {
                        // print(widget.keyListScreen);
                        // Navigator.of(context).pop();
                        // Navigator.of(context).pop();
                        // Toast.show("Xóa thành công!", context,
                        //     duration: Toast.LENGTH_SHORT,
                        //     gravity: Toast.BOTTOM);
                        // BlocProvider.of<ManageBloc>(context)
                        //     .add(DeleteAccountEvent(widget.user.idUser));
                      },
                    );
                  });
            },
            color: Colors.black,
          ),
        ],
        leading: IconButton(
          icon: Icon(Icons.arrow_back_outlined),
          color: Colors.black,
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: width,
              margin: EdgeInsets.symmetric(vertical: 10),
              alignment: Alignment.center,
              child: Container(
                width: 150,
                height: 150,
                alignment: Alignment.bottomRight,
                child: Container(
                    padding: EdgeInsets.only(right: 20),
                    child: Container(
                        height: 30,
                        width: 30,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.white),
                        child: GestureDetector(
                            onTap: () {
                              showModalBottomSheet(
                                context: context,
                                builder: ((builder) => _bottomSheet()),
                              );
                            },
                            child: Icon(Icons.camera_alt)))),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: _image != null
                            ? FileImage(_image)
                            : image == null || image == ""
                                ? AssetImage('assets/default_image.jpg')
                                : NetworkImage(image),
                        fit: BoxFit.cover)),
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: Text("Tên bài viết"),
            ),
            Container(
              height: 80,
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
              ),
              child: CustomTextField.edit(
                txtController: _titleNews,
                hintText: '${widget.news.title}',
                keyboardType: TextInputType.text,
                onChanged: (String value) {
                  // check();
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: Text("Mô tả"),
            ),
            Container(
              height: 120,
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
              ),
              child: CustomTextField.edit(
                txtController: _shortContentNews,
                hintText: '${widget.news.shortContent}',
                keyboardType: TextInputType.text,
                onChanged: (String value) {
                  // check();
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
              child: Text("Nội dung"),
            ),
            Container(
              height: 120,
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
              ),
              child: CustomTextField.edit(
                txtController: _shortContentNews,
                hintText: '${widget.news.content}',
                keyboardType: TextInputType.text,
                onChanged: (String value) {
                  // check();
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: CustomButton('Chỉnh sửa', () {
                // if (_fistName.text == '' &&
                //     _lastName.text == '' &&
                //     _phoneNumber.text == '' &&
                //     selectGender == widget.user.gender &&
                //     selectUserType == widget.user.userType) {
                //   _scaffoldKey.currentState.showSnackBar(SnackBar(
                //       content: Text("Bạn phải thay đổi mới có thể cập nhật")));
                //   return;
                // }
                // var user = widget.user;
                // User userInfo = User(
                //     idUser: user.idUser,
                //     firstName:
                //         _fistName.text == '' ? user.firstName : _fistName.text,
                //     lastName:
                //         _lastName.text == '' ? user.lastName : _lastName.text,
                //     gender: selectGender,
                //     userType: selectUserType,
                //     phoneNumber: _phoneNumber.text == ''
                //         ? user.phoneNumber
                //         : int.parse(_phoneNumber.text));

                // showDialog(
                //     context: context,
                //     builder: (_) {
                //       return CustomDialog.product(
                //         image: user.userThumbnail,
                //         name: user.lastName,
                //         actionText: "cập nhật",
                //         onConfirm: () {
                //           Navigator.of(context).pop();
                //           Toast.show("Cập nhật thành công!", context,
                //               duration: Toast.LENGTH_SHORT,
                //               gravity: Toast.BOTTOM);
                //           BlocProvider.of<ManageBloc>(context)
                //               .add(UpdateAccountEvent(userInfo));
                //           Navigator.of(context).pop();
                //         },
                //       );
                //     });
              }, Colors.blue),
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  Widget _bottomSheet() {
    return Container(
      height: 100,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Column(
        children: [
          Text(
            "Choose Profile Photo",
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              FlatButton.icon(
                onPressed: getImageFromCamera,
                icon: Icon(Icons.camera),
                label: Text("Camera"),
              ),
              FlatButton.icon(
                onPressed: getImageFromGallery,
                icon: Icon(Icons.image),
                label: Text("Galery"),
              )
            ],
          )
        ],
      ),
    );
  }
}
