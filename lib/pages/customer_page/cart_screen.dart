import 'package:basketballShop/modules/shopping_cart/bloc/cart_bloc.dart';
import 'package:basketballShop/modules/shopping_cart/widgets/cart_footer.dart';
import 'package:basketballShop/modules/shopping_cart/widgets/cart_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<CartBloc, CartState>(
        builder: (context, state) {
          if (state is CartInitial) {
            return Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset('assets/design.svg'),
                  Text("Vui lòng đăng nhập để  thực hiện chức năng này"),
                ],
              ),
            );
          }
          return Column(
            children: [CartList(), CartFooter()],
          );
        },
      ),
    );
  }
}
