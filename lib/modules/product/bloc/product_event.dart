part of 'product_bloc.dart';

@immutable
abstract class ProductEvent {}

class FirstLoadProductEvent extends ProductEvent {}

class LoadMoreProductEvent extends ProductEvent {}

class DeleteProductEvent extends ProductEvent {
  final int index;
  DeleteProductEvent(this.index);
}

class RelatedProductEvent extends ProductEvent {
  final int idProduct;
  RelatedProductEvent(this.idProduct);
}
