import 'dart:async';

import 'package:basketballShop/const/baseUrl.dart';
import 'package:basketballShop/modules/product/models/product.dart';
import 'package:basketballShop/modules/product/repository/repository_product.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'product_event.dart';
part 'product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  ProductBloc() : super(ProductInitial());

  @override
  Stream<ProductState> mapEventToState(
    ProductEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is FirstLoadProductEvent) {
      yield ProductInitial();
      // await Future.delayed(Duration(seconds: 2));
      var getProduct = await RepositoryProduct().getProduct();
      var getShoes = await RepositoryProduct()
          .getProductByCate(API_GET_PRODUCT_BY_ID + 'shoe');
      var getJerseys = await RepositoryProduct()
          .getProductByCate(API_GET_PRODUCT_BY_ID + 'jersey');
      var getAccessories = await RepositoryProduct()
          .getProductByCate(API_GET_PRODUCT_BY_ID + 'accessory');
      var getBasketball = await RepositoryProduct()
          .getProductByCate(API_GET_PRODUCT_BY_ID + 'basketball');
      yield ProductSuccessLoad(
          products: getProduct.results,
          shoes: getShoes.results,
          jerseys: getJerseys.results,
          accessories: getAccessories.results,
          basketball: getBasketball.results,
          relatedProduct: []);
    }

    if (event is RelatedProductEvent) {
      var getRelatedProduct =
          await RepositoryProduct().getRelatedProduct(event.idProduct);
      List<Product> relatedProduct = getRelatedProduct.results;
      yield ProductSuccessLoad(
          products: state.products,
          shoes: state.shoes,
          jerseys: state.jerseys,
          accessories: state.accessories,
          basketball: state.basketball,
          relatedProduct: relatedProduct);
    }
  }
}
