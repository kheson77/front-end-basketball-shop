import 'package:get/state_manager.dart';

class CategoryTabbarController extends GetxController {
  var index = 0.obs;
  void change(var index) {
    this.index.value = index;
  }
}
