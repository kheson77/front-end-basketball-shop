import 'package:basketballShop/modules/manage/bloc/manage_bloc.dart';
import 'package:basketballShop/modules/manage/widget/manage_dashboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Manager extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        child: RefreshIndicator(
          onRefresh: () async {
            BlocProvider.of<ManageBloc>(context).add(GetAllManageEvent());
          },
          child: Column(
            children: [
              SizedBox(
                height: height * 0.05,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: Text(
                        "Quản lý",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 28,
                            color: Colors.black),
                      )),
                  Container(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: Icon(Icons.search)),
                ],
              ),
              SizedBox(height: 15),
              ManageDashboard()
            ],
          ),
        ),
      ),
    );
  }
}
