part of 'product_bloc.dart';

@immutable
abstract class ProductState {
  final List<Product> products;
  final List<Product> shoes;
  final List<Product> jerseys;
  final List<Product> accessories;
  final List<Product> basketball;
  final List<Product> relatedProduct;
  ProductState(
      {this.products,
      this.shoes,
      this.jerseys,
      this.accessories,
      this.basketball,
      this.relatedProduct});

  int get getAllProduct {
    if (products == null || products.length == 0) {
      return 0;
    }
    return products.length;
  }
}

class ProductInitial extends ProductState {}

class ProductLoadMore extends ProductState {
  ProductLoadMore(List<Product> products) : super(products: products);
}

class ProductSuccessLoad extends ProductState {
  ProductSuccessLoad(
      {List<Product> products,
      List<Product> shoes,
      List<Product> jerseys,
      List<Product> accessories,
      List<Product> basketball,
      List<Product> relatedProduct})
      : super(
            products: products,
            shoes: shoes,
            jerseys: jerseys,
            accessories: accessories,
            basketball: basketball,
            relatedProduct: relatedProduct);
}
