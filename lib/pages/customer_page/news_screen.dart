import 'package:basketballShop/modules/news/bloc/news_bloc.dart';
import 'package:basketballShop/modules/news/widgets/news_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NewsScreen extends StatefulWidget {
  @override
  _NewsScreenState createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        child: RefreshIndicator(
          onRefresh: () async {
            BlocProvider.of<NewsBloc>(context).add(FirstLoadNews());
          },
          child: Column(
            children: [
              SizedBox(
                height: height * 0.05,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: Text(
                        "Tin Tức",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 28,
                            color: Colors.black),
                      )),
                  Container(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: Icon(Icons.search)),
                ],
              ),
              NewsList()
            ],
          ),
        ),
      ),
    );
  }
}
