import 'dart:async';

import 'package:basketballShop/modules/login/bloc/login_bloc.dart';
import 'package:basketballShop/modules/shopping_cart/models/cart.dart';
import 'package:basketballShop/modules/shopping_cart/repository/repository_cart.dart';
import 'package:basketballShop/modules/product/models/product.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'cart_event.dart';
part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  // CartBloc() : super(CartInitial());
  final LoginBloc loginBloc;
  StreamSubscription loginSubscription;
  String accessToken;
  int idUser;
  CartBloc({@required this.loginBloc}) : super(CartInitial()) {
    void onLoginStateChanged(state) {
      if (state is LoginSuccessful) {
        add(LoadedCart(state.user.idUser, state.user.accessToken));
        idUser = state.user.idUser;
        accessToken = state.user.accessToken;
        // print("${idUser} : ${accessToken}");
      } else if (state is LoginFailed) {
        add(FirstLoadCartEvent());
        print("Dang xuat");
      }
    }

    onLoginStateChanged(loginBloc.state);
    loginSubscription = loginBloc.stream.listen(onLoginStateChanged);
  }

  @override
  Stream<CartState> mapEventToState(
    CartEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is FirstLoadCartEvent) {
      // print("event is CartInitial");
      yield CartInitial();
    }
    if (event is LoadedCart) {
      // print("event is LoadedCart");
      yield CartInitial();
      yield* _mapFirstLoadCartEventToState(event);
    }
    if (event is AddItemEvent) {
      yield CartLoading(state.carts);
      yield* _mapAddItemEventToState(event);
    }
    if (event is DeleteItemEvent) {
      yield CartLoading(state.carts);
      yield* _mapDeleteItemEventToState(event);
    }
    if (event is IncreaseItemEvent) {
      yield CartLoading(state.carts);
      yield* _mapIncreaseItemEventToState(event);
    }
    if (event is DecreaseItemEvent) {
      yield CartLoading(state.carts);
      yield* _mapDecreaseItemEventToState(event);
    }
  }

  Stream<CartState> _mapFirstLoadCartEventToState(LoadedCart event) async* {
    try {
      List<Cart> carts = [];
      var getCart =
          await RepositoryCart().getCart(event.idUser, event.accessToken);
      carts.addAll(getCart.results);

      yield CartSuccess(carts);
    } catch (_) {}
  }

  Stream<CartState> _mapAddItemEventToState(AddItemEvent event) async* {
    try {
      // print("${idUser} : ${accessToken}");
      List<Cart> carts = [];
      var getCart;

      // Kiểm tra item đã tồn tại hay chưa
      bool exist = false;
      for (int i = 0; i < state.carts.length; i++) {
        // Nếu tồn tại, tăng số lượng lên 1
        if (state.carts[i].idProduct == event.product.idProduct) {
          await RepositoryCart().updateCart(state.carts[i].idCart, accessToken,
              state.carts[i].quantity + event.count);
          exist = true;
          break;
        }
      }
      // Thêm item vào giỏ hàng
      if (!exist) {
        await RepositoryCart()
            .addCart(event.product, accessToken, idUser, event.count);
      }

      getCart = await RepositoryCart().getCart(idUser, accessToken);
      carts.addAll(getCart.results);
      yield CartSuccess(carts);
    } catch (_) {}
  }

  Stream<CartState> _mapDeleteItemEventToState(DeleteItemEvent event) async* {
    try {
      List<Cart> carts = [];
      var getCart;
      await RepositoryCart().deleteCart(event.index, accessToken);
      getCart = await RepositoryCart().getCart(idUser, accessToken);
      carts.addAll(getCart.results);
      yield CartSuccess(carts);
    } catch (_) {}
  }

  Stream<CartState> _mapIncreaseItemEventToState(
      IncreaseItemEvent event) async* {
    try {
      List<Cart> carts = [];
      var getCart;
      await RepositoryCart()
          .updateCart(event.cart.idCart, accessToken, event.cart.quantity + 1);
      getCart = await RepositoryCart().getCart(idUser, accessToken);
      carts.addAll(getCart.results);
      yield CartSuccess(carts);
    } catch (_) {}
  }

  Stream<CartState> _mapDecreaseItemEventToState(
      DecreaseItemEvent event) async* {
    try {
      List<Cart> carts = [];
      var getCart;
      await RepositoryCart()
          .updateCart(event.cart.idCart, accessToken, event.cart.quantity - 1);
      getCart = await RepositoryCart().getCart(idUser, accessToken);
      carts.addAll(getCart.results);
      yield CartSuccess(carts);
    } catch (_) {}
  }

  @override
  Future<void> close() {
    loginSubscription.cancel();
    return super.close();
  }
}
