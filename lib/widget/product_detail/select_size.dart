import 'package:flutter/material.dart';

class SelectSize extends StatefulWidget {
  @override
  _SelectSizeState createState() => _SelectSizeState();
}

class _SelectSizeState extends State<SelectSize> {
  int _indexSize;
  @override
  void initState() {
    super.initState();
    _indexSize = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 5, left: 24, right: 24),
      child: Row(
        children: [
          InkWell(
            onTap: () {
              setState(() {
                _indexSize = 0;
              });
            },
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                border: Border.all(
                    width: 2,
                    color: _indexSize == 0 ? Colors.blue : Colors.grey),
                borderRadius: BorderRadius.circular(10),
                color: _indexSize == 0 ? Colors.blue : Colors.transparent,
              ),
              alignment: Alignment.center,
              child: Text(
                "41",
                style: TextStyle(
                    fontSize: 16,
                    color: _indexSize == 0 ? Colors.white : Colors.black),
              ),
            ),
          ),
          SizedBox(width: 15),
          InkWell(
            onTap: () {
              setState(() {
                _indexSize = 1;
              });
            },
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                border: Border.all(
                    width: 2,
                    color: _indexSize == 1 ? Colors.blue : Colors.grey),
                borderRadius: BorderRadius.circular(10),
                color: _indexSize == 1 ? Colors.blue : Colors.transparent,
              ),
              alignment: Alignment.center,
              child: Text(
                "43",
                style: TextStyle(
                    fontSize: 16,
                    color: _indexSize == 1 ? Colors.white : Colors.black),
              ),
            ),
          ),
          SizedBox(width: 15),
          InkWell(
            onTap: () {
              setState(() {
                _indexSize = 2;
              });
            },
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                border: Border.all(
                    width: 2,
                    color: _indexSize == 2 ? Colors.blue : Colors.grey),
                borderRadius: BorderRadius.circular(10),
                color: _indexSize == 2 ? Colors.blue : Colors.transparent,
              ),
              alignment: Alignment.center,
              child: Text(
                "44",
                style: TextStyle(
                    fontSize: 16,
                    color: _indexSize == 2 ? Colors.white : Colors.black),
              ),
            ),
          ),
          SizedBox(width: 15),
          InkWell(
            onTap: () {
              setState(() {
                _indexSize = 3;
              });
            },
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                border: Border.all(
                    width: 2,
                    color: _indexSize == 3 ? Colors.blue : Colors.grey),
                borderRadius: BorderRadius.circular(10),
                color: _indexSize == 3 ? Colors.blue : Colors.transparent,
              ),
              alignment: Alignment.center,
              child: Text(
                "46",
                style: TextStyle(
                    fontSize: 16,
                    color: _indexSize == 3 ? Colors.white : Colors.black),
              ),
            ),
          )
        ],
      ),
    );
  }
}
