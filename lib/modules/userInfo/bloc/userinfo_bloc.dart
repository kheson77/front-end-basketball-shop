import 'dart:async';

import 'package:basketballShop/modules/login/bloc/login_bloc.dart';
import 'package:basketballShop/modules/userInfo/models/user.dart';
import 'package:basketballShop/modules/userInfo/repository/repository_user_info.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'userinfo_event.dart';
part 'userinfo_state.dart';

class UserinfoBloc extends Bloc<UserinfoEvent, UserinfoState> {
  // UserinfoBloc() : super(UserinfoInitial());

  final LoginBloc loginBloc;
  StreamSubscription loginSubscription;
  String accessToken;
  int idUser;
  UserinfoBloc({@required this.loginBloc}) : super(UserinfoInitial()) {
    void onLoginStateChanged(state) {
      print(state);
      if (state is LoginSuccessful) {
        print('Dang nhap vao day');
        idUser = state.user.idUser;
        accessToken = state.user.accessToken;
        add(UserInfoLoaded());
      } else if (state is LoginFailed) {
        add(UserInfoFirstLoadEvent());
        print("Dang xuat");
      }
    }

    onLoginStateChanged(loginBloc.state);
    loginSubscription = loginBloc.stream.listen(onLoginStateChanged);
  }

  @override
  Stream<UserinfoState> mapEventToState(
    UserinfoEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is UserInfoFirstLoadEvent) {
      print('firstload');
      yield UserinfoInitial();
    }
    if (event is UserInfoLoaded) {
      print('load');
      yield UserinfoInitial();
      yield* _mapUserInfoLoadedToState(event);
    }
    if (event is UserInfoUpdateInfoEvent) {
      yield UserInfoLoad(state.user);
      yield* _mapUserInfoUpdateInfoEventToState(event);
    }
    if (event is UserInfoUpdateImageEvent) {
      yield UserInfoLoad(state.user);
      yield* _mapUserInfoUpdateImageEventToState(event);
    }
  }

  Stream<UserinfoState> _mapUserInfoLoadedToState(UserInfoLoaded event) async* {
    var getUser = await RepositoryUserInfo().getUserById(idUser, accessToken);
    yield UserInfoSuccess(getUser);
  }

  Stream<UserinfoState> _mapUserInfoUpdateInfoEventToState(
      UserInfoUpdateInfoEvent event) async* {
    await RepositoryUserInfo().updateUserInfo(idUser, accessToken, event.user);
    var getUser = await RepositoryUserInfo().getUserById(idUser, accessToken);
    yield UserInfoSuccess(getUser);
  }

  Stream<UserinfoState> _mapUserInfoUpdateImageEventToState(
      UserInfoUpdateImageEvent event) async* {
    await RepositoryUserInfo().updateImage(idUser, accessToken, event.image);
    // var getUser = await RepositoryUserInfo().getUserById(idUser, accessToken);
    yield UserInfoSuccess(state.user);
  }
}
