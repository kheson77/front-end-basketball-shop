import 'dart:io';

import 'package:basketballShop/widget/custom_button.dart';
import 'package:basketballShop/widget/custom_textfiled.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:image_picker/image_picker.dart';

class AddNews extends StatefulWidget {
  @override
  _AddNewsState createState() => _AddNewsState();
}

class _AddNewsState extends State<AddNews> {
  TextEditingController _titleNews = TextEditingController();
  TextEditingController _contentNews = TextEditingController();
  TextEditingController _shortContentNews = TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  File _image;
  final picker = ImagePicker();

  Future getImageFromCamera() async {
    final pickedImage =
        await picker.getImage(source: ImageSource.camera, imageQuality: 100);

    setState(() {
      if (pickedImage != null) {
        _image = File(pickedImage.path);
      } else {
        print("No Image Selected");
      }
    });
  }

  Future getImageFromGallery() async {
    final pickedImage =
        await picker.getImage(source: ImageSource.gallery, imageQuality: 100);

    setState(() {
      if (pickedImage != null) {
        _image = File(pickedImage.path);
      } else {
        print("No Image Selected");
      }
    });
  }

  final _validate =
      MultiValidator([RequiredValidator(errorText: 'Không được để trống')]);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Thêm bài viết",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        actions: [
          IconButton(
            icon: Icon(Icons.more_vert),
            onPressed: () {
              showDialog(context: context, builder: (_) {});
            },
            color: Colors.black,
          ),
        ],
        leading: IconButton(
          icon: Icon(Icons.arrow_back_outlined),
          color: Colors.black,
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: true,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: width,
                // margin: EdgeInsets.symmetric(vertical: 10),
                alignment: Alignment.center,
                child: Container(
                  width: width,
                  height: 200,
                  margin: EdgeInsets.only(left: 10, right: 10),
                  alignment: Alignment.bottomRight,
                  child: Container(
                      padding: EdgeInsets.only(right: 20, bottom: 20),
                      child: Container(
                          height: 30,
                          width: 30,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: Colors.white),
                          child: GestureDetector(
                              onTap: () {
                                showModalBottomSheet(
                                  context: context,
                                  builder: ((builder) => _bottomSheet()),
                                );
                              },
                              child: Icon(Icons.camera_alt)))),
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                          image: _image != null
                              ? FileImage(_image)
                              : AssetImage('assets/default_image.jpg'),
                          fit: BoxFit.cover)),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: Text("Tiêu đề bài viết"),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                ),
                child: CustomTextField.add(
                  validate: _validate,
                  txtController: _titleNews,
                  keyboardType: TextInputType.text,
                  onChanged: (String value) {
                    // check();
                  },
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
                child: Text("Mô tả"),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                ),
                child: CustomTextField.add(
                  validate: _validate,
                  txtController: _shortContentNews,
                  keyboardType: TextInputType.text,
                  onChanged: (String value) {
                    // check();
                  },
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
                child: Text("Nội dung bài viết"),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                height: 160,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                ),
                child: CustomTextField.add(
                  validate: _validate,
                  txtController: _contentNews,
                  keyboardType: TextInputType.text,
                  onChanged: (String value) {
                    // check();
                  },
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: CustomButton('Thêm bài viết', () {
                  if (!_formKey.currentState.validate() || _image == null) {
                    print(context);
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                        content: Text("Vui lòng không để trống các mục!")));
                  } else {
                    // final String name = _nameProduct.text;
                    // final int idCategory = selectedWard;
                    // final double price = double.parse(_priceProduct.text);
                    // final String desciption = _descriptionProduct.text;
                    // final String thumbnail = _image.path;

                    // Product product = Product(
                    //     name: name,
                    //     price: price,
                    //     description: desciption,
                    //     thumbnail: thumbnail,
                    //     idCategory: idCategory);
                    // Navigator.of(context).pop();
                    // BlocProvider.of<ManageBloc>(context)
                    //     .add(AddProductEvent(product));
                    // Toast.show("Thêm sản phẩm thành công!", context,
                    //     duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                  }
                }, Colors.blue),
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  String getCategory(int i) {
    if (i == 5) {
      return "Giày bóng rổ";
    } else if (i == 6) {
      return "Quần áo bóng rổ";
    } else if (i == 7) {
      return "Phụ kiện bóng rổ";
    } else if (i == 8) {
      return "Quả bóng rổ";
    }
  }

  Widget _bottomSheet() {
    return Container(
      height: 100,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Column(
        children: [
          Text(
            "Choose Profile Photo",
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              FlatButton.icon(
                onPressed: getImageFromCamera,
                icon: Icon(Icons.camera),
                label: Text("Camera"),
              ),
              FlatButton.icon(
                onPressed: getImageFromGallery,
                icon: Icon(Icons.image),
                label: Text("Galery"),
              )
            ],
          )
        ],
      ),
    );
  }
}
