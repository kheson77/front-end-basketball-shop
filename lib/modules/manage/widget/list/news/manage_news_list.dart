import 'package:basketballShop/Core/routes.dart';
import 'package:basketballShop/modules/manage/bloc/manage_bloc.dart';
import 'package:basketballShop/widget/loading_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';

class ManageNewsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Tin tức",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        actions: [
          IconButton(
            icon: SvgPicture.asset('assets/plus.svg'),
            onPressed: () {
              CoreRoutes.instance.navigateTo(CoreRouteNames.ADD_NEWS);
            },
            color: Colors.black,
          ),
        ],
        leading: IconButton(
          icon: Icon(Icons.arrow_back_outlined),
          color: Colors.black,
          onPressed: () {
            // Get.back();
            CoreRoutes.instance.pop();
          },
        ),
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.all(5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BlocBuilder<ManageBloc, ManageState>(
              builder: (context, state) {
                if (state is ManageInitial) {
                  return LoadingList();
                }
                return Expanded(
                  child: RefreshIndicator(
                    onRefresh: () async {
                      BlocProvider.of<ManageBloc>(context)
                          .add(GetAllManageEvent());
                    },
                    child: ListView.builder(
                        // padding: EdgeInsets.only(top: 10),
                        padding: EdgeInsets.zero,
                        itemCount: state.news.length,
                        itemBuilder: (context, i) {
                          return Container(
                            padding: EdgeInsets.only(left: 5, right: 5),
                            child: Card(
                              elevation: 2,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                                side: BorderSide(
                                  color: Colors.blue,
                                  width: 2.0,
                                ),
                              ),
                              child: InkWell(
                                onTap: () {
                                  // print(state.news[i].idNews);
                                  // BlocProvider.of<NewsBloc>(context).add(
                                  //     RelatedNewsEvent(state.news[i].idNews));
                                  CoreRoutes.instance.navigateTo(
                                      CoreRouteNames.UPDATE_NEWS,
                                      arguments: state.news[i]);
                                },
                                child: Row(
                                  children: [
                                    Container(
                                      width: width / 4,
                                      height: width / 4,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(15),
                                              bottomLeft: Radius.circular(15)),
                                          image: DecorationImage(
                                              image: NetworkImage(
                                                  state.news[i].thumbnail),
                                              fit: BoxFit.cover)),
                                    ),
                                    Container(
                                      width: width * 3 / 4 - 30,
                                      padding: EdgeInsets.only(left: 10),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "${state.news[i].title}",
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18),
                                          ),
                                          Text("ID: ${state.news[i].idNews}",
                                              style: TextStyle(fontSize: 16)),
                                          Text(
                                              "Giờ thêm: ${DateFormat('kk:mm').format(state.news[i].dateModified)}",
                                              style: TextStyle(fontSize: 16)),
                                          Text(
                                              "Ngày thêm: ${DateFormat('dd-MM-yyyy').format(state.news[i].dateModified)}",
                                              style: TextStyle(fontSize: 16)),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          );
                        }),
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
