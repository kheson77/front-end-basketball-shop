import 'package:flutter/cupertino.dart';

class CoreRouteNames {
  static const String PRODUCT_SCREEN = '/PRODUCT_SCREEN';
  static const String PRODUCT_DETAIL = '/PRODUCT_DETAIL';
  static const String HOME_SCREEN = '/HOME_SCREEN';

  static const String NEWS_DETAIL = '/NEWS_DETAIL';
  static const String REGISTER_SCREEN = '/REGISTER_SCREEN';

  static const String USER_INFO_SCREEN = "/USER_INFO_SCREEN";

  // Manage Routes
  static const String MANAGE_ACCOUNT_TAB = "/MANAGE_ACCOUNT_TAB";
  static const String MANAGE_PRODUCT_TAB = "/MANAGE_PRODUCT_TAB";
  static const String MANAGE_NEWS_LIST = "/MANAGE_NEWS_LIST";

  static const String UPDATE_PRODUCT = "/UPDATE_PRODUCT";
  static const String ADD_PRODUCT = "/ADD_PRODUCT";

  static const String UPDATE_ACCOUNT = "/UPDATE_ACCOUNT";

  static const String UPDATE_NEWS = "/UPDATE_NEWS";
  static const String ADD_NEWS = "/ADD_NEWS";
}

class CoreRoutes {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  factory CoreRoutes() => _instance;

  CoreRoutes._internal();

  static final CoreRoutes _instance = CoreRoutes._internal();

  static CoreRoutes get instance => _instance;

  String curentRoutes = '';

  Future<dynamic> navigateTo(String routeName, {Object arguments}) async =>
      navigatorKey.currentState.pushNamed(routeName, arguments: arguments);

  Future<dynamic> navigateAndRemove(String routeName,
          {Object arguments}) async =>
      navigatorKey.currentState.pushNamedAndRemoveUntil(
        routeName,
        (Route<dynamic> route) => false,
        arguments: arguments,
      );

  Future<dynamic> navigateAndReplace(String routeName,
          {Object arguments}) async =>
      navigatorKey.currentState
          .pushReplacementNamed(routeName, arguments: arguments);

  dynamic pop({dynamic result}) => navigatorKey.currentState.pop(result);
}
