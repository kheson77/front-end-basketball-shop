import 'package:basketballShop/Core/routes.dart';
import 'package:basketballShop/modules/product/bloc/product_bloc.dart';
import 'package:basketballShop/widget/loading_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class LatestListProduct extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: ListTile(
                title: Text('Top danh mục sản phẩm'),
              ),
            ),
            Container(
                margin: EdgeInsets.only(right: 20),
                child: GestureDetector(
                    onTap: () {
                      //TODO: Chuyển Tab
                      // tabbarController.index.value = 1;
                    },
                    child:
                        Text('Xem thêm', style: TextStyle(color: Colors.blue))))
          ],
        ),
        // Container(
        //   height: 300,
        //   padding: EdgeInsets.all(10),
        //   child: Column(
        //     mainAxisAlignment: MainAxisAlignment.start,
        //     crossAxisAlignment: CrossAxisAlignment.start,
        //     children: [
        //       BlocBuilder<ProductBloc, ProductState>(
        //         builder: (context, state) {
        //           var products = state.shoes;
        //           if (state is ProductInitial) {
        //             return LoadingList();
        //           }
        //           return Expanded(
        //             child: ListView.builder(
        //                 // padding: EdgeInsets.only(top: 10),
        //                 itemCount: 3,
        //                 itemBuilder: (context, i) {
        //                   return Card(
        //                     elevation: 2,
        //                     shape: RoundedRectangleBorder(
        //                       borderRadius: BorderRadius.only(
        //                           topLeft: Radius.circular(15),
        //                           bottomLeft: Radius.circular(15)),
        //                     ),
        //                     child: InkWell(
        //                       // padding: EdgeInsets.only(bottom: 10),
        //                       borderRadius: BorderRadius.circular(8),
        //                       splashColor: Colors.orange,
        //                       onTap: () {
        //                         BlocProvider.of<ProductBloc>(context).add(
        //                             RelatedProductEvent(products[i].idProduct));
        //                         CoreRoutes.instance.navigateTo(
        //                             CoreRouteNames.PRODUCT_DETAIL,
        //                             arguments: products[i]);
        //                       },
        //                       child: Row(
        //                         children: [
        //                           Hero(
        //                             tag: products[i].idProduct,
        //                             child: Container(
        //                               width: width / 4,
        //                               height: width / 3.5,
        //                               decoration: BoxDecoration(
        //                                   borderRadius: BorderRadius.only(
        //                                       topLeft: Radius.circular(15),
        //                                       bottomLeft: Radius.circular(15)),
        //                                   image: DecorationImage(
        //                                       image: NetworkImage(
        //                                           products[i].thumbnail),
        //                                       fit: BoxFit.cover)),
        //                             ),
        //                           ),
        //                           Container(
        //                             width: width * 3 / 4 - 30,
        //                             padding: EdgeInsets.only(left: 10),
        //                             child: Column(
        //                               mainAxisAlignment:
        //                                   MainAxisAlignment.start,
        //                               crossAxisAlignment:
        //                                   CrossAxisAlignment.start,
        //                               children: [
        //                                 Text(
        //                                   "${products[i].name}",
        //                                   style: TextStyle(
        //                                       fontWeight: FontWeight.bold),
        //                                 ),
        //                                 Text(
        //                                   "${products[i].price} đ",
        //                                   style: TextStyle(color: Colors.red),
        //                                 ),
        //                                 Text(
        //                                   "${products[i].description}",
        //                                   maxLines: 1,
        //                                   overflow: TextOverflow.ellipsis,
        //                                 ),
        //                                 SmoothStarRating(
        //                                   rating: 4.5,
        //                                   size: 15,
        //                                   color: Colors.amber,
        //                                   borderColor: Colors.amber,
        //                                   starCount: 5,
        //                                 ),
        //                               ],
        //                             ),
        //                           )
        //                         ],
        //                       ),
        //                     ),
        //                   );
        //                 }),
        //           );
        //         },
        //       )
        //     ],
        //   ),
        // ),
      ],
    );
  }
}
