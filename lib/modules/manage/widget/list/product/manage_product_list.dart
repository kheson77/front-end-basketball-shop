import 'package:basketballShop/Core/routes.dart';
import 'package:basketballShop/modules/manage/bloc/manage_bloc.dart';
import 'package:basketballShop/modules/product/models/product.dart';
import 'package:basketballShop/widget/loading_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class ManageProductList extends StatelessWidget {
  final List<Product> products;

  ManageProductList({this.products});

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.all(5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BlocBuilder<ManageBloc, ManageState>(
              builder: (context, state) {
                if (state is ManageInitial) {
                  return LoadingList();
                }
                return Expanded(
                  child: RefreshIndicator(
                    onRefresh: () async {
                      BlocProvider.of<ManageBloc>(context)
                          .add(GetAllManageEvent());
                    },
                    child: ListView.builder(
                        // padding: EdgeInsets.only(top: 10),
                        padding: EdgeInsets.zero,
                        itemCount: products.length,
                        itemBuilder: (context, i) {
                          var image = products[i].thumbnail;
                          return Container(
                            padding: EdgeInsets.only(left: 5, right: 5),
                            child: Card(
                              elevation: 2,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                                side: BorderSide(
                                  color: products[i].status == 'active'
                                      ? Colors.blue
                                      : Colors.red,
                                  width: 2.0,
                                ),
                              ),
                              child: InkWell(
                                onTap: () {
                                  // print(state.news[i].idNews);
                                  // BlocProvider.of<NewsBloc>(context).add(
                                  //     RelatedNewsEvent(state.news[i].idNews));
                                  CoreRoutes.instance.navigateTo(
                                      CoreRouteNames.UPDATE_PRODUCT,
                                      arguments: products[i]);
                                },
                                child: Row(
                                  children: [
                                    Container(
                                      width: width / 4,
                                      height: width / 4,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(15),
                                              bottomLeft: Radius.circular(15)),
                                          image: DecorationImage(
                                              image: image == null ||
                                                      image == ""
                                                  ? AssetImage(
                                                      'assets/default_image.jpg')
                                                  : NetworkImage(image),
                                              fit: BoxFit.cover)),
                                    ),
                                    Container(
                                      width: width * 2.5 / 4,
                                      padding: EdgeInsets.only(left: 10),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "${products[i].name}",
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18),
                                          ),
                                          Text(
                                              "Mã sản phẩm: ${products[i].idProduct}",
                                              style: TextStyle(fontSize: 14)),
                                          // Text(
                                          //     "Giờ chỉnh sửa: ${DateFormat('kk:mm').format(products[i].modifiedDate)}",
                                          //     style: TextStyle(fontSize: 14)),
                                          // Text(
                                          //     "Ngày chỉnh sửa: ${DateFormat('dd-MM-yyyy').format(products[i].modifiedDate)}",
                                          //     style: TextStyle(fontSize: 14)),
                                          Text("Số lượng: " +
                                              products[i].count.toString()),
                                          RichText(
                                            text: TextSpan(
                                              text: 'Trạng thái: ',
                                              style: TextStyle(
                                                  color: Colors.black),
                                              children: <TextSpan>[
                                                TextSpan(
                                                    text: getStatus(
                                                        products[i].status),
                                                    style: TextStyle(
                                                        color: products[i]
                                                                    .status ==
                                                                'active'
                                                            ? Colors.blue
                                                            : Colors.red)),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        }),
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }

  String getStatus(String s) {
    if (s == 'active') {
      return 'Hoạt động';
    } else {
      return 'Không hoạt động';
    }
  }
}
