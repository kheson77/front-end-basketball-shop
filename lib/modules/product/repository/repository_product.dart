import 'package:basketballShop/const/baseUrl.dart';
import 'package:basketballShop/modules/product/models/product.dart';
import 'package:dio/dio.dart';

class RepositoryProduct {
  Future<ProductModel> getProduct() async {
    Response response = await Dio().get(API_GET_PRODUCT);
    if (response.statusCode == 200) {
      // print(response.data);
      return ProductModel.fromJson(response.data);

      // Tea tea = Tea.fromJson(response.data);
    } else
      return null;
  }

  Future<ProductModel> getProductByCate(String url) async {
    Response response = await Dio().get(url);
    if (response.statusCode == 200) {
      // print(response.data);
      return ProductModel.fromJson(response.data);

      // Tea tea = Tea.fromJson(response.data);
    } else
      return null;
  }

  Future<ProductModel> getRelatedProduct(int idProduct) async {
    Response response =
        await Dio().get("${API_GET_RELATED_PRODUCT}${idProduct}");
    if (response.statusCode == 200) {
      // print(response.data);
      return ProductModel.fromJson(response.data);

      // Tea tea = Tea.fromJson(response.data);
    } else
      return null;
  }
}
