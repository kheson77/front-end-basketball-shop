import 'package:basketballShop/modules/shopping_cart/bloc/cart_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CartFooter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return BlocBuilder<CartBloc, CartState>(
      builder: (context, state) {
        final cart = state;
        return Container(
          width: width,
          margin: EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 5),
                child: Row(
                  children: [
                    Text(
                      "Tổng sản phẩm:",
                      style: TextStyle(fontSize: 16),
                    ),
                    Spacer(),
                    Text("${cart.totalCart} sản phẩm",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 5),
                child: Row(
                  children: [
                    Text("Tổng số lượng:", style: TextStyle(fontSize: 16)),
                    Spacer(),
                    Text("${cart.totalProduct}",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 5),
                child: Row(
                  children: [
                    Text("Tổng tiền:", style: TextStyle(fontSize: 16)),
                    Spacer(),
                    Text("${cart.totalPrice} đồng",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
              Center(
                child: Container(
                  width: width / 2.5,
                  child: ElevatedButton(
                    onPressed: () {
                      // UserDA da = UserDA();
                      // da.getUser();
                    },
                    child: Text(
                      'Đặt hàng',
                      // style: TextStyle(fontSize: 18),
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
