const String dev = "http://10.0.2.2:3000/";
const String deploy = "https://sonkhe-basketball-shop.herokuapp.com/";

const String domain = dev;

// API User
const API_REGISTER = "${domain}api/user/register";
const API_LOGIN = "${domain}api/user/login";
const API_GET_USER = "${domain}api/user/";
const API_GET_PRODUCT = "${domain}api/product";
const API_UPDATE_IMAGE_USER = "${domain}api/user/updateImage/"; // + $id
const API_UPDATE_INFO_USER = "${domain}api/user/"; // + $id

// API Customer
const API_GET_CART_BY_ID = "${domain}api/cart/byIdUser/";
const API_GET_CART = "${domain}api/cart";
const API_POST_CART = "${domain}api/cart/";
const API_GET_USER_BY_ID = "${domain}api/user/"; // + $id

// API Mangager
const API_UPDATE_PRODUCT = "${domain}api/product/";
const API_DELETE_ACCOUNT = "${domain}api/user/"; // + $id

// API non-User
const API_GET_PRODUCT_BY_ID = "${domain}api/product/byNameCate/";
const API_GET_ALL_PRODUCT = "${domain}api/product/all";
const API_GET_RELATED_PRODUCT = "${domain}api/product/related/";
const API_GET_NEWS = "${domain}api/news";
const API_GET_RELATED_NEWS = "${domain}api/news/related/";
