import 'package:flutter/material.dart';

class HeaderHomeScreen extends StatefulWidget {
  @override
  _HeaderHomeScreenState createState() => _HeaderHomeScreenState();
}

class _HeaderHomeScreenState extends State<HeaderHomeScreen> {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Container(
      height: 120,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        children: [
          Container(
              height: 100,
              child: AppBar(
                // leading: Icon(Icons.ac_unit_outlined),
                // title: Text("Trang chủ",
                //     style: TextStyle(fontSize: 16.0, color: Colors.black)),
                centerTitle: true,
                flexibleSpace: Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    height: 130,
                    width: width,
                    //     color: Colors.red.withOpacity(1)
                    child: Image.network(
                      "https://image.freepik.com/free-vector/online-shopping-banner_82574-3393.jpg",
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              )),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 40,
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 20, right: 20),
              child: TextField(
                style: TextStyle(
                  fontSize: 13,
                ),
                onChanged: (val) {},
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  contentPadding: EdgeInsets.zero,
                  prefixIcon: GestureDetector(
                    child: Icon(Icons.search),
                    onTap: () {},
                  ),
                  hintText: "Bạn muốn mua gì?",
                  border: OutlineInputBorder(
                    borderSide: BorderSide.none,
                  ),
                  suffixIcon: GestureDetector(
                    child: Icon(
                      Icons.close,
                    ),
                    onTap: () {},
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
