import 'package:basketballShop/Core/routes.dart';
import 'package:basketballShop/const/color_custom.dart';
import 'package:basketballShop/modules/login/bloc/login_bloc.dart';
import 'package:basketballShop/modules/userInfo/widgets/dashboard_screen.dart';
import 'package:basketballShop/widget/custom_button.dart';
import 'package:basketballShop/widget/login/input_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:form_field_validator/form_field_validator.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController user = TextEditingController();
  final TextEditingController pass = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _validateEmail = MultiValidator([
    EmailValidator(
      errorText: 'Email không hợp lệ',
    ),
    RequiredValidator(errorText: 'Email không được để trống')
  ]);
  final _validatePass = MultiValidator([
    RequiredValidator(errorText: 'Password không được để trống'),
    MinLengthValidator(6, errorText: 'Nhập ít nhất 6 ký tự')
  ]);

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    var heightKeyboard = MediaQuery.of(context).viewInsets.bottom;
    // final LoginBloc bloc = BlocProvider.of<LoginBloc>(context);
    return BlocBuilder<LoginBloc, LoginState>(
      builder: (context, state) {
        if (state is LoginSuccessful) {
          return DashBoardScreen();
        }
        if (state is LoginLoad) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        return Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.white,
          resizeToAvoidBottomInset: true,
          body: SingleChildScrollView(
            child: Stack(
              children: [
                Positioned(
                  top: 130,
                  left: 60,
                  width: width * 0.6,
                  child: Image.asset('assets/login.png'),
                ),
                Container(
                  height: height - 80,
                  width: width,
                  child: Form(
                    key: _formKey,
                    autovalidate: true,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: height * 0.08,
                        ),
                        Padding(
                            padding: EdgeInsets.only(left: 20, right: 20),
                            child: Text(
                              "Đăng nhập",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 28,
                                  color: Colors.black),
                            )),
                        Container(
                          padding:
                              EdgeInsets.only(top: 15, left: 20, right: 20),
                          child: Text(
                            "Vui lòng đăng nhập để có thể đặt ngay các sản phẩm của chúng tôi",
                            style:
                                TextStyle(fontSize: 15.5, color: Colors.black),
                          ),
                        ),
                        SizedBox(
                          height: 190,
                        ),
                        InputText.email(
                            hint_text: "Email",
                            label_text: "Email",
                            textEditingController: user,
                            validator: _validateEmail),
                        InputText.password(
                            hint_text: "Mật khẩu",
                            label_text: "Mật khẩu",
                            textEditingController: pass,
                            validator: _validatePass),
                        Container(
                          padding: EdgeInsets.only(top: 10, right: 20),
                          alignment: Alignment.centerRight,
                          child: Text("Quên mật khẩu",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.black)),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 15),
                          child: CustomButton("Đăng nhập", () {
                            if (_formKey.currentState.validate()) {
                              final String username = user.text;
                              final String password = pass.text;
                              BlocProvider.of<LoginBloc>(context).add(
                                  LoginSubmitted(
                                      username: username, password: password));
                            } else {
                              _scaffoldKey.currentState.showSnackBar(SnackBar(
                                  content: Text(
                                      "Vui lòng điền đúng thông tin đăng nhập!")));
                            }
                          }, Colors.purple),
                        ),
                        Container(
                          padding: EdgeInsets.only(bottom: 30, top: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("Bạn chưa có tài khoản?",
                                  style: TextStyle(
                                      fontSize: 15, color: Colors.black)),
                              InkWell(
                                onTap: () {
                                  CoreRoutes.instance.navigateTo(
                                      CoreRouteNames.REGISTER_SCREEN);
                                },
                                child: Text(" Đăng ký ngay",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.blue)),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
