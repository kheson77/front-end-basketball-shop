import 'package:basketballShop/Core/routes.dart';
import 'package:basketballShop/modules/manage/bloc/manage_bloc.dart';
import 'package:basketballShop/modules/manage/widget/list/product/manage_product_list.dart';
import 'package:basketballShop/modules/product/models/product.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ManageProductTab extends StatefulWidget {
  @override
  _ManageProductTabState createState() => _ManageProductTabState();
}

class _ManageProductTabState extends State<ManageProductTab> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Sản phẩm",
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
          actions: [
            IconButton(
              padding: EdgeInsets.only(right: 20),
              icon: SvgPicture.asset('assets/plus.svg'),
              onPressed: () {
                CoreRoutes.instance.navigateTo(CoreRouteNames.ADD_PRODUCT);
              },
              color: Colors.black,
            ),
          ],
          leading: IconButton(
            icon: Icon(Icons.arrow_back_outlined),
            color: Colors.black,
            onPressed: () {
              // Get.back();
              CoreRoutes.instance.pop();
            },
          ),
          elevation: 0,
          bottom: const TabBar(
            labelColor: Colors.black,
            unselectedLabelColor: Colors.grey,
            isScrollable: true,
            tabs: <Widget>[
              Tab(
                text: "Giày bóng rổ",
              ),
              Tab(
                text: "Đồng phục",
              ),
              Tab(
                text: "Phụ kiện",
              ),
              Tab(
                text: "Quả bóng rổ",
              ),
            ],
          ),
        ),
        body: BlocBuilder<ManageBloc, ManageState>(
          builder: (context, state) {
            return TabBarView(
              children: [
                ManageProductList(
                  products: state.shoes,
                ),
                ManageProductList(
                  products: state.jerseys,
                ),
                ManageProductList(
                  products: state.accessories,
                ),
                ManageProductList(
                  products: state.basketball,
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
