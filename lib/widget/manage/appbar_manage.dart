import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AppBarManage extends StatefulWidget implements PreferredSizeWidget {
  final String title;
  final VoidCallback onPressAdd;
  final List<Widget> tab;

  AppBarManage({
    @required this.title,
    @required this.onPressAdd,
  }) : this.tab = null;

  AppBarManage.withTab(
      {@required this.title, @required this.onPressAdd, @required this.tab});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(
        "Sản phẩm",
        style: TextStyle(color: Colors.black),
      ),
      backgroundColor: Colors.white,
      actions: [
        IconButton(
          icon: Icon(Icons.add),
          onPressed: () {},
          color: Colors.black,
        ),
      ],
      leading: IconButton(
        icon: SvgPicture.asset('assets/plus.svg'),
        color: Colors.black,
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
      elevation: 0,
      bottom: tab == null
          ? null
          : TabBar(
              labelColor: Colors.black,
              unselectedLabelColor: Colors.grey,
              isScrollable: true,
              tabs: tab),
    );
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    throw UnimplementedError();
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => const Size.fromHeight(100);
}
