import 'dart:async';

import 'package:basketballShop/modules/news/models/newsModel.dart';
import 'package:basketballShop/modules/news/repository/repository_news.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'news_event.dart';
part 'news_state.dart';

class NewsBloc extends Bloc<NewsEvent, NewsState> {
  NewsBloc() : super(NewsInitial());

  @override
  Stream<NewsState> mapEventToState(
    NewsEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is FirstLoadNews) {
      yield NewsInitial();
      List<News> news = [];
      var getNews = await RepositoryNews().getNews();
      news.addAll(getNews.results);
      yield NewsSuccessLoaded(news: news, relatedNews: []);
    }

    if (event is RelatedNewsEvent) {
      // yield NewsInitial();
      var getRelatedNews = await RepositoryNews().getRelatedNews(event.idNews);
      List<News> relatedNews = getRelatedNews.results;
      yield NewsSuccessLoaded(news: state.news, relatedNews: relatedNews);
    }
  }
}
