import 'dart:async';

import 'package:basketballShop/modules/login/model/user.dart';
import 'package:basketballShop/modules/login/repository/login_repository.dart';
import 'package:basketballShop/modules/tabbar/tabbar_controller.dart';
import 'package:bloc/bloc.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginInitial());

  TabbarController tabbarController = Get.find();
  var accessToken;

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is LoginSubmitted) {
      yield LoginLoad();
      // await Future.delayed(Duration(milliseconds: 1500));
      var res = await LoginRepository().login(event.username, event.password);
      if (res != null) {
        print(res);
        yield LoginSuccessful(res, res.userType);
        if (res.userType == "manager") {
          tabbarController.index.value = 2;
        } else {
          tabbarController.index.value = 4;
        }
      } else {
        yield LoginFailed();
      }
    }
    if (event is LoginFirstLoadEvent) {
      yield LoginInitial();
    }
    if (event is LogoutEvent) {
      tabbarController.index.value = 4;
      yield LoginFailed();
    }
    // if (event is UpdateImage) {
    //   User user;
    //   print('123');
    //   LoginRepository()
    //       .updateImage(state.user.idUser, event.image)
    //       .then((value) async {
    //     user = await LoginRepository()
    //         .getUserById(state.user.idUser, state.user.accessToken);
    //   });

    //   user.accessToken = state.user.accessToken;
    //   yield LoginSuccessful(user, user.userType);
    // }
  }
}
