import 'package:basketballShop/const/color_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class InputText extends StatefulWidget {
  @override
  _InputTextState createState() => _InputTextState();

  final String hint_text;
  final String label_text;
  final TextEditingController textEditingController;
  final bool show_password;
  final Function validator;
  final String assetName;

  InputText.name({
    @required this.hint_text,
    @required this.label_text,
    @required this.textEditingController,
    this.validator,
  })  : this.show_password = null,
        this.assetName = 'assets/user.svg';

  InputText.email({
    @required this.hint_text,
    @required this.label_text,
    @required this.textEditingController,
    this.validator,
  })  : this.show_password = null,
        this.assetName = 'assets/mail.svg';

  InputText.password({
    @required this.hint_text,
    @required this.label_text,
    @required this.textEditingController,
    this.validator,
  })  : this.show_password = false,
        this.assetName = 'assets/unlock.svg';
}

class _InputTextState extends State<InputText> {
  bool _showPassword;

  @override
  void initState() {
    super.initState();
    _showPassword = widget.show_password;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 90,
      padding: EdgeInsets.only(left: 20, top: 8, right: 20),
      child: TextFormField(
        validator: widget.validator,
        maxLength: 30,
        // autovalidateMode: AutovalidateMode.always,
        controller: widget.textEditingController,
        obscureText: this._showPassword == null ? false : !this._showPassword,
        decoration: InputDecoration(
            labelStyle: TextStyle(color: Colors.black, fontSize: 14),
            hintStyle: TextStyle(fontSize: 14),
            labelText: widget.label_text,
            hintText: widget.label_text,
            border: OutlineInputBorder(),
            // focusedBorder: OutlineInputBorder(
            //   borderRadius: BorderRadius.circular(4),
            //   borderSide: BorderSide(color: Colors.black, width: 2.0),
            // ),
            // enabledBorder: OutlineInputBorder(
            //   borderRadius: BorderRadius.circular(4),
            //   borderSide: BorderSide(color: Colors.grey, width: 2.0),
            // ),
            fillColor: Colors.white,
            filled: true,
            prefixIcon: Container(
              height: 10,
              width: 10,
              alignment: Alignment.center,
              child: SvgPicture.asset(
                widget.assetName,
                color: primaryColor,
                height: 18,
                width: 18,
              ),
            ),
            suffixIcon: _showPassword == null
                ? SizedBox()
                : IconButton(
                    icon: Icon(
                      this._showPassword
                          ? Icons.visibility
                          : Icons.visibility_off,
                      color: this._showPassword ? Colors.black : Colors.grey,
                    ),
                    onPressed: () {
                      setState(() => this._showPassword = !this._showPassword);
                    },
                  )),
      ),
    );
  }
}
