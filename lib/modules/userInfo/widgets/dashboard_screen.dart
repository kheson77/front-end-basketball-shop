import 'dart:io';

import 'package:basketballShop/Core/routes.dart';
import 'package:basketballShop/modules/login/bloc/login_bloc.dart';
import 'package:basketballShop/modules/userInfo/bloc/userinfo_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class DashBoardScreen extends StatefulWidget {
  @override
  _DashBoardScreenState createState() => _DashBoardScreenState();
}

class _DashBoardScreenState extends State<DashBoardScreen> {
  File _image;

  final picker = ImagePicker();

  Future getImageFromCamera() async {
    final pickedImage =
        await picker.getImage(source: ImageSource.camera, imageQuality: 100);

    setState(() {
      if (pickedImage != null) {
        _image = File(pickedImage.path);
        print(_image.absolute);
        BlocProvider.of<UserinfoBloc>(context)
            .add(UserInfoUpdateImageEvent(pickedImage.path));
      } else {
        print("No Image Selected");
      }
    });
  }

  Future getImageFromGallery() async {
    final pickedImage =
        await picker.getImage(source: ImageSource.gallery, imageQuality: 100);

    setState(() {
      if (pickedImage != null) {
        _image = File(pickedImage.path);
        print(_image.absolute);
        BlocProvider.of<UserinfoBloc>(context)
            .add(UserInfoUpdateImageEvent(pickedImage.path));
      } else {
        print("No Image Selected");
      }
    });
  }

  @override
  void initState() {
    super.initState();
  }

  List<String> _menuOptions = [
    "Thông tin cá nhân",
    "Lịch sử đặt hàng",
    "Trợ giúp",
    "Đăng xuất"
  ];

  List<String> _iconOption = [
    'assets/user.svg',
    'assets/order.svg',
    'assets/setting.svg',
    'assets/logout.svg'
  ];

  List<String> _routeOption = [
    CoreRouteNames.USER_INFO_SCREEN,
    CoreRouteNames.USER_INFO_SCREEN,
    CoreRouteNames.USER_INFO_SCREEN,
  ];

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return BlocBuilder<UserinfoBloc, UserinfoState>(
      builder: (context, state) {
        if (state is UserinfoInitial) {
          return Center(
            child: RefreshProgressIndicator(),
          );
        } else {
          var image = state.user.userThumbnail;
          return RefreshIndicator(
            onRefresh: () {
              BlocProvider.of<UserinfoBloc>(context).add(UserInfoLoaded());
            },
            child: SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: Container(
                height: height,
                width: width,
                color: Colors.white,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: height * 0.08,
                    ),
                    Center(
                      child: Container(
                        width: width,
                        padding: EdgeInsets.all(15),
                        child: Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                showModalBottomSheet(
                                  context: context,
                                  builder: ((builder) => _bottomSheet()),
                                );
                              },
                              child: Container(
                                margin: EdgeInsets.only(right: 10),
                                width: 100,
                                height: 100,
                                alignment: Alignment.bottomRight,
                                child: Container(
                                  padding: EdgeInsets.only(right: 5),
                                  child: Container(
                                      height: 30,
                                      width: 30,
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white),
                                      child: Icon(Icons.camera_alt)),
                                ),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                      image: _image != null
                                          ? FileImage(_image)
                                          : image == null || image == ""
                                              ? AssetImage(
                                                  "assets/avt_default.png",
                                                )
                                              : NetworkImage(image),
                                      fit: BoxFit.cover,
                                    )),
                              ),
                            ),
                            Container(
                              width: width * 0.5,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    // width: width,
                                    margin: EdgeInsets.only(top: 0),
                                    child: Text(
                                      "${state.user.lastName}",
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize: 24,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Container(
                                    // width: width,
                                    margin: EdgeInsets.only(top: 5),
                                    child: Text(
                                      state.user.userType == 'customer'
                                          ? "Người dùng"
                                          : "Quản lý",
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w300),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 5),
                                    child: SmoothStarRating(
                                      rating: 3.5,
                                      size: 15,
                                      color: Colors.amber,
                                      borderColor: Colors.amber,
                                      starCount: 5,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Spacer(),
                            Container(
                              height: 100,
                              alignment: Alignment.topRight,
                              padding: EdgeInsets.all(10),
                              child: Icon(
                                Icons.edit,
                                size: 25,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: height * 0.02),
                    Expanded(
                        // height: height * 0.4,
                        child: ListView.builder(
                      itemCount: _menuOptions.length,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () async {
                            if (index == 3) {
                              buildShowDialog(context);
                              await Future.delayed(Duration(seconds: 1))
                                  .then((value) {
                                Navigator.of(context).pop();
                                BlocProvider.of<LoginBloc>(context)
                                    .add(LogoutEvent());
                              });
                            } else {
                              CoreRoutes().navigateTo(_routeOption[index]);
                            }
                          },
                          child: Container(
                            // color: primaryColor,
                            height: 60,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                border:
                                    Border.all(color: Colors.blue, width: 2)),
                            margin: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 5),
                            padding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 10),
                            child: Row(
                              children: [
                                Container(
                                    child: SvgPicture.asset(
                                  _iconOption[index],
                                  height: 20,
                                )),
                                Container(
                                  width: width / 1.6,
                                  padding: EdgeInsets.only(left: 20),
                                  // height: 50,
                                  child: Text(
                                    "${_menuOptions[index]}",
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.black),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                      physics: NeverScrollableScrollPhysics(),
                    )),
                  ],
                ),
              ),
            ),
          );
        }
      },
    );
  }

  buildShowDialog(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  Widget _bottomSheet() {
    return Container(
      height: 100,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Column(
        children: [
          Text(
            "Lựa chọn ảnh cá nhân",
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              FlatButton.icon(
                onPressed: getImageFromCamera,
                icon: Icon(Icons.camera),
                label: Text("Camera"),
              ),
              FlatButton.icon(
                onPressed: getImageFromGallery,
                icon: Icon(Icons.image),
                label: Text("Galery"),
              )
            ],
          ),
        ],
      ),
    );
  }
}
