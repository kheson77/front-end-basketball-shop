import 'dart:convert';

import 'package:basketballShop/const/baseUrl.dart';
import 'package:basketballShop/modules/userInfo/models/user.dart';
import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:http/http.dart' as http;

class RepositoryUserInfo {
  Future<User> getUserById(int id, String accessToken) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = accessToken;
    Response response = await dio.get("${API_GET_USER}${id}");
    if (response.statusCode == 200) {
      print(response.data);
      return User.fromJson(response.data);
    } else {
      return null;
    }
  }

  Future<User> updateImage(int id, String accessToken, String image) async {
    var userThumbnail = await http.MultipartFile.fromPath(
        "userThumbnail", image,
        contentType: MediaType("image", "png"));
    var request = http.MultipartRequest(
        "PATCH", Uri.parse('${API_UPDATE_IMAGE_USER}${id}'));
    request.headers["Authorization"] = accessToken;
    request.headers["Content-Type"] = "multipart/form-data";
    request.files.add(userThumbnail);
    request.send().then((response) async {
      print(response.statusCode);
      Dio dio = new Dio();
      dio.options.headers["Authorization"] = accessToken;
      Response res = await dio.get("${API_GET_USER}${id}");
      if (res.statusCode == 200) {
        print(res.data);
        return User.fromJson(res.data);
      } else {
        return null;
      }
      print("Cap nhat anh thanh cong");
    }).catchError((error) => print(error));
  }

  Future<void> updateUserInfo(int id, String accessToken, User user) async {
    final encoding = Encoding.getByName('utf-8');
    Map body = {
      "firstName": user.firstName,
      "lastName": user.lastName,
      "gender": user.gender,
      "phoneNumber": user.phoneNumber
    };

    var res = await http.patch(Uri.parse('${API_UPDATE_INFO_USER}${id}'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': accessToken
        },
        body: json.encode(body),
        encoding: encoding);
    if (res.statusCode == 200) {
      print(res.body);
      print('Cap nhat user thanh cong');
    } else
      return null;
  }
}
