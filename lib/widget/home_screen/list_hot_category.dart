import 'package:basketballShop/modules/tabbar/category_tabbar_controller.dart';
import 'package:basketballShop/modules/tabbar/tabbar_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListHotCategory extends StatelessWidget {
  TabbarController tabbarController = Get.find();
  CategoryTabbarController categoryTabbarController = Get.find();
  List<String> categories = [
    "Giày bóng rổ",
    "Đồng phục",
    "Phụ kiện",
    "Quả bóng rổ"
  ];

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: ListTile(
                title: Text('Top danh mục sản phẩm'),
              ),
            ),
            Container(
                margin: EdgeInsets.only(right: 20),
                child: GestureDetector(
                    onTap: () {
                      //TODO: Chuyển Tab
                      tabbarController.index.value = 1;
                    },
                    child: Text('Xem thêm',
                        style: TextStyle(color: Colors.blue)))),
          ],
        ),
        Container(
          height: 150,
          margin: EdgeInsets.only(left: 10, right: 10),
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: 4,
            itemBuilder: (context, i) {
              return InkWell(
                onTap: () {
                  categoryTabbarController.index.value = i;
                  tabbarController.index.value = 1;
                },
                child: Container(
                  margin: EdgeInsets.only(left: 10, right: 10),
                  child: Column(
                    children: [
                      Container(
                        width: width / 4,
                        height: 100,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: NetworkImage(
                                    "https://www.lsretail.com/hubfs/BLOG_6-technology-trends-reshaping-luxury-fashion-industry.jpg"),
                                fit: BoxFit.cover)),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5),
                        child: Text("${categories[i]}"),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
