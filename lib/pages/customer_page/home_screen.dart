import 'package:basketballShop/modules/news/bloc/news_bloc.dart';
import 'package:basketballShop/widget/home_screen/header_home_screen.dart';
import 'package:basketballShop/widget/home_screen/latest_list_product.dart';
import 'package:basketballShop/widget/home_screen/list_hot_category.dart';
import 'package:basketballShop/widget/home_screen/list_hot_news.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
          // margin: EdgeInsets.only(top: 28),
          child: RefreshIndicator(
        onRefresh: () async {
          BlocProvider.of<NewsBloc>(context).add(FirstLoadNews());
        },
        child: CustomScrollView(
          slivers: [
            SliverList(
                delegate: SliverChildListDelegate([
              HeaderHomeScreen(),
              ListHotNews(),
              ListHotCategory(),
              Container(
                  margin: EdgeInsets.only(left: 10, right: 10),
                  child: Image.asset(
                    'assets/ads.png',
                    width: width,
                    height: 120,
                    fit: BoxFit.cover,
                  )),
              ListHotCategory(),
              LatestListProduct()
            ]))
          ],
        ),
      )),
    );
  }
}
