part of 'login_bloc.dart';

@immutable
abstract class LoginState {
  final User user;
  String userType = "customer";
  LoginState({this.user, this.userType});
}

class LoginInitial extends LoginState {}

class LoginLoad extends LoginState {}

class LoginSuccessful extends LoginState {
  final User user;
  LoginSuccessful(this.user, String userType)
      : super(user: user, userType: userType);
}

class LoginFailed extends LoginState {}
