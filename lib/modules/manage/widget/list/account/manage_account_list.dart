import 'package:basketballShop/Core/routes.dart';
import 'package:basketballShop/modules/manage/bloc/manage_bloc.dart';
import 'package:basketballShop/modules/manage/model/account.dart';
import 'package:basketballShop/modules/userInfo/models/user.dart';
import 'package:basketballShop/widget/custom_dialog.dart';
import 'package:basketballShop/widget/loading_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class ManageAccountList extends StatelessWidget {
  final List<Account> accounts;
  ManageAccountList({this.accounts});
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BlocBuilder<ManageBloc, ManageState>(
            builder: (context, state) {
              if (state is ManageInitial) {
                return LoadingList();
              }
              return Expanded(
                child: RefreshIndicator(
                  onRefresh: () async {
                    BlocProvider.of<ManageBloc>(context)
                        .add(GetAllManageEvent());
                  },
                  child: ListView.builder(
                      // padding: EdgeInsets.only(top: 10),
                      padding: EdgeInsets.zero,
                      itemCount: accounts.length,
                      itemBuilder: (context, i) {
                        var image = accounts[i].userThumbnail;
                        return Container(
                          padding: EdgeInsets.only(left: 5, right: 5),
                          child: Card(
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                              side: BorderSide(
                                color: Colors.blue,
                                width: 2.0,
                              ),
                            ),
                            child: InkWell(
                              onTap: () {
                                User user = User(
                                    idUser: accounts[i].idUser,
                                    email: accounts[i].email,
                                    firstName: accounts[i].firstName,
                                    lastName: accounts[i].lastName,
                                    gender: accounts[i].gender,
                                    phoneNumber: accounts[i].phoneNumber,
                                    userThumbnail: accounts[i].userThumbnail,
                                    userType: accounts[i].userType,
                                    modifiedDate: accounts[i].modifiedDate);
                                CoreRoutes.instance.navigateTo(
                                    CoreRouteNames.UPDATE_ACCOUNT,
                                    arguments: user);
                              },
                              child: Row(
                                children: [
                                  Container(
                                    width: width / 4,
                                    height: width / 4,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(15),
                                            bottomLeft: Radius.circular(15)),
                                        image: DecorationImage(
                                            image: image == null
                                                ? AssetImage(
                                                    'assets/avt_default.png')
                                                : NetworkImage(image),
                                            fit: BoxFit.cover)),
                                  ),
                                  Container(
                                    width: width * 2.2 / 4,
                                    padding: EdgeInsets.only(left: 10),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "${accounts[i].email}",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18),
                                        ),
                                        Text("ID: ${accounts[i].idUser}",
                                            style: TextStyle(fontSize: 16)),
                                        Text(
                                            "Giờ thêm: ${DateFormat('kk:mm').format(accounts[i].modifiedDate)}",
                                            style: TextStyle(fontSize: 16)),
                                        Text(
                                            "Ngày thêm: ${DateFormat('dd-MM-yyyy').format(accounts[i].modifiedDate)}",
                                            style: TextStyle(fontSize: 16)),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    height: width / 4,
                                    alignment: Alignment.topRight,
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
